<link rel="stylesheet" href="<?=base_url('assets/') ?>backend/vendor/jasny-bootstrap/css/jasny-bootstrap.min.css">

<div class="container-fluid" id="container-wrapper">
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <div>
      <h1 class="h3 mb-0 text-gray-800"><?=$title ?></h1>
      <p>Catatan Bobin Besar</p>
      <a href="<?=site_url('administrador/material-stok') ?>" class="btn btn-sm btn-outline-primary"><i class="fa fa-angle-left"></i> Kembali</a>
    </div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="./">CU</a></li>
      <li class="breadcrumb-item">Order Stok</li>
      <li class="breadcrumb-item active">Bobin Besar</li>
    </ol>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="card card-body mb-3">
        <form enctype='multipart/form-data' class="mb-3" action="#" method="POST">
          <div class="form-row">
            <div class="col">
              <label>Export Excel (<strong>csv, xls</strong>)</label>
              <div class="custom-file">
                <input type="file" name="import_file" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
              </div>
            </div>
            <div class="col">
              <button type="submit" id="upload" name="upload" style="margin-top: 32px" class="btn btn-outline-secondary">Upload</button>
            </div>
          </div>
        </form>
      </div>

      <form action="<?=site_url('administrador/material-stok/type/cu/' .$material['slug']) ?>" 
        method="POST">
        <div class="row">
          <div class="col-sm-8">
            <div class="card mb-4 shadow">
              <div class="card-body">

                <div class="row">
                  <div class="col-sm-3">
                    <div class="form-group">
                      <label for="total_bobin" class="text-primary">Total Bobin</label>
                      <input type="text" id="total_bobin" value="<?=$material['total_bobin'] ?>" name="total_bobin" readonly class="form-control" value="">
                    </div>
                  </div>
                  <div class="col-sm-9">
                    <div class="form-group">
                      <label for="total_stok" class="text-primary">Total Stok</label>
                      <div class="input-group">
                        <input type="text" id="total_stok" readonly name="total_stok" class="form-control" value="<?=$material['stok'] ?>"> 
                        <span class="input-group-append"> 
                          <div class="input-group-text">Kg</div>
                        </span>
                      </div>       
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="text-primary">Bobin Besar</label>
                  <div class="input-group input-spinner">
                    <div class="input-group-prepend">
                      <button class="input-group-text" type="button" id="button-minus"> 
                        <i class="fa fa-minus"></i> 
                      </button>
                    </div>

                    <input type="text" id="qty" name="qty" class="form-control text-center" value="<?=$material['total_bobin'] ?>">

                    <div class="input-group-append">
                      <button class="input-group-text" type="button" id="button-plus"> 
                        <i class="fa fa-plus"></i> 
                      </button>
                    </div>
                  </div> <!-- input-group.// -->
                </div>  

                <?php $this->db->select('no_bobin, bruto, berat_bobin') ?>
                <?php $bobin = $this->db->get_where('material_kawat', 
                  ['material_kawat_stok_id' => $material['id']])->result() ?>

                <table class="table table-striped table-shopping-cart">
                  <thead class="text-muted">
                    <tr>
                      <th scope="col" width="120">No Bobin <br><?=form_error('no_bobin[]', '<small class="text-danger">', '</small>') ?></th>
                      <th scope="col" width="200">Berat Bobin <br><?=form_error('berat_bobin[]', '<small class="text-danger">', '</small>') ?></th>
                      <th scope="col" width="200">Bruto <br><?=form_error('bruto[]', '<small class="text-danger">', '</small>') ?></th>
                    </tr>
                  </thead>
                  <tbody id="field_wrapper">
                    <?php if($bobin) : ?>
                      <?php foreach($bobin as $b) : ?>
                      <tr>
                        <td>
                          <input type="text" value="<?=$b->no_bobin ?>" readonly class="form-control" name="no_bobin[]">
                        </td>
                        <td> 
                          <div class="input-group">
                            <input type="text" class="form-control" name="berat_bobin[]" value="<?=$b->berat_bobin ?>">
                            <span class="input-group-append"> 
                              <div class="input-group-text">Kg</div>
                            </span>
                          </div>
                        </td>
                        <td> 
                          <div class="input-group">
                            <input type="text" class="form-control" name="bruto[]" value="<?=$b->bruto ?>">
                            <span class="input-group-append"> 
                              <div class="input-group-text">Kg</div>
                            </span>
                          </div>
                        </td>
                      </tr>
                      <?php endforeach ?>
                    <?php else: ?>
                      <?php for($i = 1; $i <= $material['total_bobin']; $i++) : ?>
                      <tr>
                        <td>
                          <input type="text" value="B<?=$i ?>" readonly class="form-control" name="no_bobin[]">
                          <?=form_error('no_bobin[]', '<small class="text-danger">', '</small>') ?>
                        </td>
                        <td> 
                          <div class="input-group">
                            <input type="text" class="form-control" name="berat_bobin[]" value="<?=set_value('berat_bobin[]') ?>">
                            <span class="input-group-append"> 
                              <div class="input-group-text">Kg</div>
                            </span>
                          </div>
                        </td>
                        <td> 
                          <div class="input-group">
                            <input type="text" class="form-control" name="bruto[]" value="<?=set_value('bruto[]') ?>">
                            <span class="input-group-append"> 
                              <div class="input-group-text">Kg</div>
                            </span>
                          </div>
                        </td>
                      </tr>
                      <?php endfor ?>
                    <?php endif ?>
                  </tbody>
                </table>
        
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card mb-4 shadow">
              <div class="card-body">
                <div class="form-group">
                  <label for="kode_supplier" class="text-primary">Nama Supplier</label>
                  <select name="kode_supplier" disabled class="form-control custom-select" id="kode_supplier">
                    <option value="">-- Select --</option>
                    <?php foreach($supplier as $sp) : ?>
                    <option value="<?=$sp['id'] ?>" 
                    <?= $material['kode_supplier'] == $sp['kode_supplier'] ? 'selected' : '' ?>><?=$sp['name'] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>

              </div>
            </div>

            <div class="card mb-4 shadow">
              <div class="card-body">

                <div class="form-group">
                  <label for="material_name" class="text-primary">Material Name</label>
                  <input type="text" readonly id="material_name" name="material_name" class="form-control" placeholder="Contoh: AF 0.50 LMK" value="<?=$material['material_name'] ?>">
                  <small class="text-muted">(Ukuran Diameter - Kode Supplier)</small> <br>
                </div> 
              </div>
            </div>

            <div class="card mb-4 shadow">
              <div class="card-footer">
                <button class="btn btn-primary btn-block" type="submit">Simpan</button>
              </div>
            </div>

          </div>
        </div>
      </form>
    </div> 
  </div>
</div>

<!-- AdminLTE App -->
<script src="<?=base_url('assets/') ?>backend/vendor/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>