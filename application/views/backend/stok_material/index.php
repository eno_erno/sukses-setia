<style>
  td.details-control {
    background: url('<?=base_url() ?>assets/backend/img/details_open.png') no-repeat center center;
    cursor: pointer
  }

  tr.details td.details-control {
    background: url('<?=base_url() ?>assets/backend/img/details_close.png') no-repeat center center;
  }

</style>

<link href="<?=base_url('assets') ?>/backend/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

<div class="container-fluid" id="container-wrapper">
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?=$title ?></h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="./">Bahan Baku</a></li>
      <li class="breadcrumb-item active">Summary</li>
    </ol>
  </div>

  <div class="card mb-3">
    <div class="card-body d-flex">
      <div class="form-inline d-inline-flex mr-auto">
        <label>Pilih Laporan</label>
      </div>
      <div class="btn-group">
        <a href="<?=site_url('administrador/material-stok') ?>" 
          class="active btn btn-outline-primary">CU</a>
        <a href="<?=site_url('administrador/material-stok/laporan/pvc') ?>" 
          class="btn btn-outline-primary">PVC</a>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <?=$this->session->flashdata('message') ?>

      <h3 class="mb-0">Summary</h3>
      <p>0.14 KMP (Ukuran Diameter - Kode Supplier)</p>
      <div class="card mb-4">
        <div class="card-header d-flex justify-content-between">
          
          <div class="form-row align-items-center">

            <div class="col-auto">
              <p class="mb-0">Pencarian : <label class="font-weight-bold">Bahan Baku</label></p>
              
              <select class="form-control custom-select form-control-sm">
                <option>0.14 mm KMP</option>
                <option>0.16 mm EWINDO</option>
                <option>0.40 mm SS</option>
                <option>1.32 mm SUTRADO</option>
              </select>
            </div>

          </div>
          <div class="card-option">
            <a class="btn btn-primary btn-sm" href="<?=site_url('administrador/order-stok/submit/cu') ?>"><i class="fa fa-truck"></i> Transaksi Baru</a>
            <a class="btn btn-info btn-sm" href="<?=site_url('administrador/bahan-baku') ?>"><i class="fa fa-database"></i> Gudang</a>
            <button class="btn btn-danger btn-sm" id="delete-summary"><i class="fa fa-trash"></i> Delete</button>
            <button class="btn btn-success btn-sm" id="excel-summary"><i class="fa fa-file-excel"></i> Export</button>
          </div>
        </div>
        <div class="card-body">
          <dl class="row">
            <dt class="col-sm-10"><span class="float-right text-muted">Stok Buncher :</span></dt>
            <dd class="col-sm-2 text-right"><strong>2005,0 Kg</strong></dd>

            <dt class="col-sm-10"><span class="float-right text-muted">Hasil Produksi : </span></dt>
            <dd class="col-sm-2 text-danger text-right"><strong>0 Kg</strong></dd>

            <dt class="col-sm-10"><span class="float-right text-muted">Stok Kawat : </span></dt>
            <dd class="col-sm-2 text-right"><strong class="h5 text-dark">14.235,38 Kg</strong></dd>
          </dl>
          <div class="table-responsive">
            <table class="table" id="dataTable-summary">
              <thead>
                <tr>
                  <th width="20">Detail</th>
                  <th scope="col" width="20">No</th>
                  <th width="100">
                    <div class="custom-control custom-checkbox small">
                      <input type="checkbox" class="custom-control-input delete-checkbox" id="select_all">
                      <label class="custom-control-label font-weight-bold" style="font-size: 16px" for="select_all">Select All</label>
                    </div>
                  </th>
                  <th scope="col">Material Name</th>
                  <th scope="col">Stok</th>
                  <th></th>
                </tr>
              </thead>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Page level plugins -->
<script src="<?=base_url('assets') ?>/backend/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url('assets') ?>/backend/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script>

  // global variable
  var manageSummaryTable;

  $(document).ready(function() {
    manageSummaryTable = $("#dataTable-summary").DataTable({
      "ajax": '<?php echo site_url('administrador/material-stok/getResultBunching')  ?>',
      "columns": [
        {
          "class": "details-control",
          "orderable": false,
          "data": null,
          "defaultContent": ""
        },
        { "data": 'id' },
        { "data": 'checkbox' },
        { "data": 'material_name' },
        { "data": 'stok_bobin' },
        { "data": 'action' }
      ],
      "orders": [],
      "ordering": false
    }) 

    function format ( d ) {
      let resulthtml = '';

      if(d.material_kawat.length > 0) {
        resulthtml += `<div class="card-body"><table class="table table-bordered table-sm">
              <thead class="border bg-light">
                <tr>
                  <th>Tanggal</th>
                  <th scope="col" width="80">No Bobin</th>
                  <th scope="col">Berat Bobin</th>
                  <th scope="col">Bruto</th>
                  <th scope="col">Netto</th>
                </tr>
              </thead>
              <tbody>`
               
              d.material_kawat.forEach((item) => {

                const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                let mydate = new Date(item.created_at);
                let formatted_date = mydate.getDate() + " " + months[mydate.getMonth()] + " " + mydate.getFullYear()

                if(item.status == 2) {
                  className = 'table-warning'
                } else if(item.status == 3) {
                  className = 'table-info'
                } else {
                  className = ''
                }

                resulthtml += `<tr class="${className}"><td>${formatted_date}</td>
                  <td width="80">${item.no_bobin}</td>
                  <td>${item.berat_bobin}</td>
                  <td>${item.bruto}</td>
                  <td>${item.netto}</td>`
              })
              
              resulthtml += `</tbody>
            </table></div>

            <p class="mt-3"><span class="text-danger">*</span> note: 
              <span class="table-warning p-1">drawing</span>
              <span class="table-info p-1">bunching</span>
            </p>`
      } else {
        resulthtml += ' <p class="text-danger">material gulungan bobin masih kosong'
      }
      

      return resulthtml;
    }

    // Array to track the ids of the details displayed rows
    var detailRows = [];
 
    $('#dataTable-summary tbody').on('click', 'tr td.details-control', function () {
      var tr = $(this).closest('tr')
      var row = manageSummaryTable.row( tr )
      var idx = $.inArray( tr.attr('id'), detailRows )

      if ( row.child.isShown() ) {
        tr.removeClass( 'details' )
        row.child.hide()

        // Remove from the 'open' array
        detailRows.splice( idx, 1 )
      }
      else {
        tr.addClass( 'details' );
        row.child( format( row.data() ) ).show()

        // Add to the 'open' array
        if ( idx === -1 ) {
          detailRows.push(tr.attr('id') )
        }
      }
    })
 
    // On each draw, loop over the `detailRows` array and show any child rows
    manageSummaryTable.on('draw', function () {
      $.each( detailRows, function (i, id) {
        $('#'+id+' td.details-control').trigger('click')
      })
    })

    $('#delete-summary').prop("disabled", true)
    $('#dataTable-summary').on('click', 'input.delete-checkbox', function() {
      if ($(this).is(':checked')) {
        $('#delete-summary').prop("disabled", false);
      } else {
        if ($('input.delete-checkbox').filter(':checked').length < 1) {
          $('#delete-summary').attr('disabled',true)
        }
      }
    })

    // Handle click on "Select all" control
    $('#select_all').on('click', function() {
      // Get all rows with search applied
      var rows = manageSummaryTable.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input.delete-checkbox[type="checkbox"]', rows).prop('checked', this.checked)
    })

    $('#delete-summary').on('click', function() {
      if( confirm("Are you sure you want to delete this?") ) {
        var data = {'bunching[]' : []}

        manageSummaryTable.$(".delete-checkbox:checked").each(function() {
          data['bunching[]'].push($(this).val())
        })

        $.post("<?=site_url('administrador/material-stok/remove-all-bobin')?>", data)
          .done(function( data ) {
          window.location.href = "<?=site_url('administrador/material-stok')?>"
        })

      } else {
        return false
      }
    })

  })
</script>