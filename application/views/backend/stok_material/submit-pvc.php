<style>
  td.details-control {
    background: url('<?=base_url() ?>assets/backend/img/details_open.png') no-repeat center center;
    cursor: pointer
  }

  tr.details td.details-control {
    background: url('<?=base_url() ?>assets/backend/img/details_close.png') no-repeat center center;
  }

</style>

<link href="<?=base_url('assets') ?>/backend/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" />

<div class="container-fluid" id="container-wrapper">
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <div>
      <h1 class="h3 mb-0 text-gray-800"><?=$title ?></h1>
      <p>Jenis PVC - Operator: <strong><?=$user['fullname'] ?></strong></p>
      <a href="<?=site_url('administrador/material-stok/laporan/pvc') ?>" class="btn btn-sm btn-outline-primary"><i class="fa fa-angle-left"></i> Kembali</a>
    </div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="./">CU</a></li>
      <li class="breadcrumb-item">Bahan Baku</li>
      <li class="breadcrumb-item active"><?= $material['pvc_name'] ?></li>
    </ol>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <form action="<?=site_url('administrador/material-stok/submit/pvc/' .$material['slug']) ?>" method="POST">
        <div class="row">
          <div class="col-sm-8">
            <div class="card mb-4 shadow">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label for="no_order" class="text-primary">No Order</label>
                      <input type="text" readonly id="no_order" name="no_order" 
                      class="form-control" value="PVC<?=random(4) ?>">
                    </div>
                  </div>
                  <div class="col-sm-8">
                    <div class="form-group">
                      <label for="title" class="text-primary">Material Bahan</label>
                      <input type="text" id="material_id" readonly name="material_id" class="form-control" 
                      value="PVC">        
                      <?=form_error('material_id', '<small class="text-danger">', '</small>') ?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="tgl_order" class="text-primary">Tanggal Order</label>
                  <input type="text" class="form-control" 
                  id="datepicker" placeholder="YY-MM-DD HH:MM" name="tgl_order">
                  <?=form_error('tgl_order', '<small class="text-danger">', '</small>') ?>
                </div>

                <div class="row">
                  <div class="col-sm-8">
                    <div class="form-group">
                      <p><label for="type_stok" class="text-primary">Laporan untuk : </label></p>
                      <select name="type_stok" disabled id="type_stok">
                       
                        <option value="incoming_stok">Masuk</option>
                        <option value="stok_out">Keluar</option>
                      </select>
                      <div class="form-text text-muted"><small>bahan pvc masuk ke gudang</small></div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <label for="result_stok" class="text-primary mt-5">Stok Digudang</label>
                  </div>
                </div>
                

                <div class="row">
                  <div class="col-sm-8">
                    <div class="form-group d-none">
                      <div class="input-group">
                        
                        <input type="text" placeholder="Barang Keluar" class="form-control" id="barang_keluar" name="barang_keluar" value="<?=set_value('barang_keluar') ?>">
                        <div class="input-group-prepend">
                          <div class="input-group-text">Kg</div>
                        </div>
                      </div>
                      
                    </div>

                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" id="barang_masuk" 
                        name="barang_masuk" placeholder="Barang Masuk" class="form-control" value="<?=set_value('barang_masuk') ?>">
                        <div class="input-group-prepend">
                          <div class="input-group-text">Kg</div>
                        </div>
                      </div>
                      <?=form_error('barang_masuk', '<small class="text-danger">', '</small>') ?>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" value="<?=$pvc_stok['stok'] ?>" readonly class="form-control" id="result_stok" name="result_stok">
                        <div class="input-group-prepend">
                          <div class="input-group-text">Kg</div>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>

                <div class="form-group">
                  <label for="type_pvc" class="text-primary">Type PVC</label>
                  <select name="type_pvc" disabled class="form-control custom-select" id="type_pvc">
                    <option value="">-- Select --</option>
                    <option value="IS" <?=$material['type_pvc'] == 'IS' ? 'selected' : '' ?>>IS</option>
                    <option value="SH" <?=$material['type_pvc'] == 'SH' ? 'selected' : '' ?>>SH</option>
                  </select>
                  <?=form_error('type_pvc', '<small class="text-danger">', '</small>') ?>
                </div>

                <div class="form-group">
                  <label for="character_pvc" class="text-primary">Character/Varian</label>
                  <input type="text" id="character_pvc" value="<?=$material['character_pvc'] ?>" placeholder="Hard, AUTOWIRE" name="character_pvc" class="form-control" readonly>
                  <?=form_error('character_pvc', '<small class="text-danger">', '</small>') ?>
                </div> 

              </div> 
            </div>

          </div>
          <div class="col-sm-4">
            <div class="card mb-4 shadow">
              <div class="card-body">
                <div class="form-group">
                  <label for="kode_supplier" class="text-primary">Dari Supplier</label>
                  <select name="kode_supplier" disabled class="form-control" id="kode_supplier">
                    <option value="">-- Select --</option>
                    <?php foreach($supplier as $sp) : ?>
                    <option value="<?=$sp['id'] ?>" 
                    <?= $material['kode_supplier'] == $sp['kode_supplier'] ? 'selected' : '' ?>><?=$sp['name'] ?></option>
                    <?php endforeach ?>
                  </select>
                  <?=form_error('kode_supplier', '<small class="text-danger">', '</small>') ?>
                </div>

              </div>
            </div>

            <div class="card mb-4 shadow">
              <div class="card-header text-primary">
                Type Warna <br>
                <small class="text-muted">Warna PVC</small> 
              </div>
              <div class="card-body">
                <div class="form-group">
                  <select name="color_id" disabled class="form-control custom-select" id="color_id">
                    <option value="">-- Select --</option>
                    <?php foreach($colors as $color) : ?>
                    <option value="<?=$color['id'] ?>" <?= $material['color_id'] == $color['id'] ? 'selected' : '' ?> <?= set_select('color_id', $color['id'], FALSE) ?>>
                      <?=$color['color_name'] ?></option>
                    <?php endforeach ?>
                  </select>
                  <?=form_error('color_id', '<small class="text-danger">', '</small>') ?>
                </div>

                <div class="form-group">
                  <label for="material_name" class="text-primary">Material Name</label>
                  <input type="text" readonly id="material_name" name="material_name" class="form-control" placeholder="Contoh: (ABC) EX317A HITAM.SH" 
                  value="<?=$material['kode_pvc'] ?> <?=$material['pvc_name'] ?>">
                  <small class="text-muted">(Kode Warna - Jenis PVC - Warna)</small> <br>
                </div> 
              </div>
            </div>

            <div class="card mb-4 shadow">
              <div class="card-body">
                <div class="form-group">
                  <label for="information" class="text-primary">Keterangan </label>
                  <textarea style="resize: none" name="information" cols="50" rows="3" id="information" class="form-control"><?=set_value('information') ?></textarea>
                  <?=form_error('information', '<small class="text-danger">', '</small>') ?>
                  <div class="form-text text-muted"><small>Maximal 160 Karakter</small></div>
                </div>

              </div>
            </div>


            <div class="card mb-4 shadow">
              <div class="card-footer">
                <button class="btn btn-primary btn-block" type="submit">Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div> 
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="card mb-4">
        <div class="card-header">
          <button class="btn btn-danger btn-sm" id="delete-laporan-stok-pvc"><i class="fa fa-trash"></i> Delete</button>
          <button class="btn btn-success btn-sm" id="excel-laporan-stok-pvc"><i class="fa fa-file-excel"></i> Export</button>
        </div>
        <div class="card-body">
          <h3 class="mb-0">Stok Bahan PVC</h3>
          <p>Type: <strong> <?= $material['pvc_name'] ?> <?= $material['kode_supplier'] ?></strong></p>
          <p>PT. SUKSES SETIA <br> 
          Jln. Kasir II No. 12 A Desa Pasir Jaya <br> 
          Jati Uwung - Tangerang</p>
          <div class="table-responsive">
            <table class="table" id="dataTable-laporan-stok-pvc">
              <thead>
                <tr>
                  <th>Detail</th>
                  <th scope="col">No</th>
                  <th width="20">
                    <div class="custom-control custom-checkbox small">
                      <input type="checkbox" class="custom-control-input delete-checkbox" id="select_all">
                      <label class="custom-control-label" for="select_all">Select All</label>
                    </div>
                  </th>
                  <th scope="col">Tanggal</th>
                  <th scope="col">No Order</th>
                  <th scope="col">Masuk <small>(Kg)</small></th>
                  <th scope="col">Keluar <small>(Kg)</small></th>
                  <th scope="col">Stok <small>(Kg)</small></th>
                  <th></th>
                </tr>
              </thead>
            </table>
          </div>
          <div class="card-footer">
            <mark>Note: </mark>
            <small class="text-danger">Laporan stok bahan pvc.</small>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<!-- AdminLTE App -->
<!-- Page level plugins -->
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script src="<?=base_url('assets') ?>/backend/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url('assets') ?>/backend/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script>
  $(document).ready(function() {
    <?php 
      $id = $material['id']; 
      $url = site_url('administrador/material-stok/getStokBahanPVC/'.$id);
    ?>

    // global variable
    var manageLaporanPVCTable;

    $(document).ready(function() {
      manageLaporanPVCTable = $("#dataTable-laporan-stok-pvc").DataTable({
        "ajax": '<?php echo $url ?>',
        "ordering": false,
        "columns": [
          {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": ""
          },
          { "data": 'id' },
          { "data": 'checkbox' },
          { "data": 'tgl_order' },
          { "data": 'no_order' },
          { "data": 'incoming_stok' },
          { "data": 'stok_out' },
          { "data": 'result_stok' },
          { "data": 'action' }
        ],
      }) 

      function format ( d ) {
        return `<p>Keterangan : </p> <strong>${d.information}</strong>`
      }

      // Array to track the ids of the details displayed rows
      var detailRows = [];
   
      $('#dataTable-laporan-stok-pvc tbody').on('click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr')
        var row = manageLaporanPVCTable.row( tr )
        var idx = $.inArray( tr.attr('id'), detailRows )

        if ( row.child.isShown() ) {
          tr.removeClass( 'details' )
          row.child.hide()

          // Remove from the 'open' array
          detailRows.splice( idx, 1 )
        }
        else {
          tr.addClass( 'details' );
          row.child( format( row.data() ) ).show()

          // Add to the 'open' array
          if ( idx === -1 ) {
            detailRows.push(tr.attr('id') )
          }
        }
      })
   
      // On each draw, loop over the `detailRows` array and show any child rows
      manageLaporanPVCTable.on('draw', function () {
        $.each( detailRows, function (i, id) {
          $('#'+id+' td.details-control').trigger('click')
        })
      })
    })

    $('#delete-laporan-stok-pvc').prop("disabled", true)
    $('#dataTable-laporan-stok-pvc').on('click', 'input.delete-checkbox', function() {
      if ($(this).is(':checked')) {
        $('#delete-laporan-stok-pvc').prop("disabled", false);
      } else {
        if ($('input.delete-checkbox').filter(':checked').length < 1) {
          $('#delete-laporan-stok-pvc').attr('disabled',true)
        }
      }
    })

    // Handle click on "Select all" control
    $('#select_all').on('click', function() {
      // Get all rows with search applied
      var rows = manageLaporanPVCTable.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input.delete-checkbox[type="checkbox"]', rows).prop('checked', this.checked)
    })

    $('#delete-laporan-stok-pvc').on('click', function() {
      if( confirm("Are you sure you want to delete this?") ) {
        var data = {'laporanpvc[]' : []}

        manageLaporanPVCTable.$(".delete-checkbox:checked").each(function() {
          data['laporanpvc[]'].push($(this).val())
        })

        $.post("<?=site_url('administrador/material-stok/remove-all-laporan-pvc')?>", data)
          .done(function( data ) {
          window.location.href = "<?=site_url('administrador/material-stok')?>"
        })

      } else {
        return false
      }
    })

    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'yyyy-mm-dd',
      footer: true, 
      modal: true
    })
  })
</script>