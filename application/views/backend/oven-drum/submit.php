
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" />
<style>
  .input-group-append .text-shadow-none, .input-group-prepend .text-shadow-none {
  	box-shadow: none !important;
  	border: 1px solid #ced4da
  }
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.min.css">

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-2">
    <div class="heading">
      <h1 class="h3 mb-0 text-gray-800"><?=$title ?></h1>
    </div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="./">Submit</a></li>
      <li class="breadcrumb-item active">Drawing Bobin</li>
    </ol>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="card">
      <div class="card-body d-flex">
        <div class="form-inline d-inline-flex mr-auto">
          <label>Category
          <select disabled class="ml-2 form-control custom-select">
            <option>CU</option>
          </select>
        </label></div>
      </div>
      </div>
    </div> 
  </div> 

  <div class="row mt-3 mb-3">

    <aside class="col-md-3 mb-3">
      <!--   SIDEBAR   -->
      <ul class="list-group mb-3 d-flex justify-content-between">
        <a class="list-group-item" href="<?=site_url('administrador/bahan-baku') ?>"><i class="fa fa-warehouse"></i> Gudang </a>
        <a class="list-group-item" href="<?=site_url('administrador/bahan-baku/drawing') ?>"><i class="fa fa-chevron-right"></i> Drawing </a>
        <!-- <a class="list-group-item active" href="<?=site_url('administrador/oven-drum/submit') ?>"><i class="fa fa-sync"></i> Submit Drawing </a> -->
      </ul>

      <a class="btn btn-outline-light btn-block active" href="<?=site_url('administrador/bahan-baku/oven-drum') ?>"> 
        <i class="fa fa-table"></i> <span class="text">Oven Drum</span> 
      </a> 

      <a href="" class="btn btn-outline-light btn-block">
        <div class="d-flex align-items-center">
          <strong>Loading...</strong>
          <div class="spinner-border spinner-border-sm ml-auto"></div>
        </div>
      </a>

      <ul class="list-group mt-3 d-flex justify-content-between">
        <a class="list-group-item" href="<?=site_url('administrador/bahan-baku/bunching') ?>"><i class="fa fa-fill"></i> Bunching </a>
      </ul>
      <!--   SIDEBAR .//END   -->
    </aside>

    <main class="col-md-9">
    	<form action="" method="POST">
				<div class="card">
					
					<div class="card-body">
						<header class="mb-4">
							<h4 class="card-title mb-0">Submit Oven</h4>
							<p>Untuk proses pemanasan bobin</p>
							<a href="<?=site_url('administrador/bahan-baku/oven-drum') ?>" class="btn btn-sm btn-outline-primary"><i class="fa fa-chevron-left"></i> Kembali </a>
						</header>

						<div class="form-group">
	          	<label class="text-primary" for="no_machine">Drum</label>
	          	<select id="no_machine" class="js-example-basic-multiple form-control" name="no_machine">
		            <?php foreach($type_mesin as $oven) : ?>
		              <option value="<?=strtolower($oven->no_machine) ?>"><?=$oven->type_machine ?> 
		              </option>
		            <?php endforeach ?>
	          	</select>
	          </div>

	          <div class="form-group">
	          	<label class="text-primary">Hasil Drawing 
	          	<small class="text-muted">yg sudah ditarik</small></label>
	            <select name="material_name" class="form-control custom-select">
	              <option value="">Select</option>
	              <?php foreach($type_bahanbaku as $tb) : ?>
	              <option value="<?=$tb['id'] ?>" 
	              <?= set_select('material_name', $tb['id'], FALSE) ?>><?=$tb['drawing'] ?> - <?=$tb['material_drawing'] ?> (<?=$tb['total_netto'] ?> kg)</option>
	              <?php endforeach ?>
	            </select>
	            <?=form_error('material_name', '<small class="text-danger">', '</small>') ?>
	          </div>

	          <!-- <div class="form-group">
	          	<label class="text-primary" for="drawing">Nomor Drawing</label>
	          	<select id="drawing" class="js-example-basic-multiple form-control" name="drawing[]" multiple>
		            <option>Drawing 1</option>
		            <option>Drawing 2</option>
		            <option>Drawing 3</option>
		            <option>Drawing 4</option>
		            <option>Drawing 5</option>
		            <option>Drawing 6</option>
		            <option>Drawing 7</option>
	          	</select>
	          </div> -->
						
						<label class="text-primary">Mendapatkan Hasil Berapa Bobin Kecil?</label>
						<div class="input-group input-spinner">
							<div class="input-group-prepend">
								<button class="btn btn-outline-light text-shadow-none" type="button" id="button-minus"> 
									<i class="fa fa-minus"></i> 
								</button>
							</div>

							<input type="text" id="qty" class="form-control text-center" value="1">

							<div class="input-group-append">
								<button class="btn btn-outline-light text-shadow-none" type="button" id="button-plus"> 
									<i class="fa fa-plus"></i> 
								</button>
							</div>
						</div> <!-- input-group.// -->
					</div>

					<table class="table table-borderless table-shopping-cart">
						<thead class="text-muted">
							<tr class="small text-uppercase">
							  <th scope="col" width="200">No Bobin <br><?=form_error('no_bobin[]', '<small class="text-danger">', '</small>') ?></th>
							  <th scope="col" >Berat Bobin <br><?=form_error('berat_bobin[]', '<small class="text-danger">', '</small>') ?></th>
							  <th scope="col" width="200">Bruto <br><?=form_error('bruto[]', '<small class="text-danger">', '</small>') ?></th>
							  <th scope="col" class="text-right" width="80"> </th>
							</tr>
						</thead>
						<tbody id="field_wrapper">
							<tr>
								<td>
									<div class="input-group">
										<input type="text" class="form-control" name="no_bobin[]" value="<?=set_value('no_bobin[]') ?>" placeholder="No Bobin">
										<!-- <span class="input-group-append"> 
											<button class="btn text-shadow-none">Kg</button>
										</span> -->
									</div>
								</td>
								<td> 
									<div class="input-group">
										<input type="text" class="form-control" name="berat_bobin[]" value="<?=set_value('berat_bobin[]') ?>" placeholder="Berat Bobin">
										<span class="input-group-append"> 
											<button class="btn text-shadow-none">Kg</button>
										</span>
									</div>
								</td>
								<td> 
									<div class="input-group">
										<input type="text" class="form-control" value="<?=set_value('bruto[]') ?>" name="bruto[]" placeholder="Bruto">
										<span class="input-group-append"> 
											<button class="btn text-shadow-none">Kg</button>
										</span>
									</div>
								</td>
								<td class="text-right"> 
									<a href="" class="btn btn-outline-light remove_button"> <i class="fa fa-trash"></i></a>
								</td>
							</tr>
						</tbody>
					</table>

					<div class="card-footer">
						<button type="submit" class="btn btn-block btn-outline-primary">Simpan</button>
					</div>

				</div> <!-- card.// -->
			</form>

    </main>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
  $(document).ready(function() {

  	$('.js-example-basic-multiple').select2({
      theme: 'bootstrap4'
    })

    var maxField = 20; //input fields increment limitation
	  var buttonPlus = $('#button-plus'); //add button selector
	  var buttonMinus = $('#button-minus'); //add button selector
	  var wrapper = $('#field_wrapper'); //input field wrapper

	  var fieldHTML = `<tr>
							<td>
								<div class="input-group">
									<input type="text" class="form-control" name="no_bobin[]" placeholder="No Bobin">
								</div>
							</td>
							<td> 
								<div class="input-group">
									<input type="text" class="form-control" name="berat_bobin[]" placeholder="Berat Bobin">
									<span class="input-group-append"> 
										<button class="btn text-shadow-none">Kg</button>
									</span>
								</div>
							</td>
							<td> 
								<div class="input-group">
									<input type="text" class="form-control" name="bruto[]" placeholder="Bruto">
									<span class="input-group-append"> 
										<button class="btn text-shadow-none">Kg</button>
									</span>
								</div>
							</td>
							<td class="text-right"> 
								<a href="" class="btn btn-outline-light remove_button"> <i class="fa fa-trash"></i></a>
							</td>
						</tr>`; //new input field html 


	  var x = $('#qty').val() //initial field counter is 1
	  
	  //once add button is clicked
	  $(buttonPlus).click(function(e) {
	    //Check maximum number of input fields
	    if(x < maxField){ 
	      x++ //Increment field counter
	      $(wrapper).append(fieldHTML) //Add field html
	      $('#qty').val(x)
	    }
	  })

	  $(buttonMinus).click(function(e) {
	    //check minimun number of input fields
	    if(x > 1) { 
	      $( "tbody tr:nth-child("+x+")" ).remove() //add field html
	      x-- //Increment field counter
	      $('#qty').val(x)
	    }
	  })
	  
	  //once remove button is clicked
	  $(wrapper).on('click', '.remove_button', function(e) {
	    e.preventDefault()
	    $(this).parent().parent().remove() //remove field html
	    x-- //decrement field counter
	  })

  })
</script>