<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" />
<style>
  .input-group-prepend span {
    background-color: #fff;
    border: 1px solid #ced4da;
    color: #495057;
    box-shadow: none !important
  }
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.min.css">
<link href="<?=base_url('assets') ?>/backend/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-2">
    <div class="heading">
      <h1 class="h3 mb-0 text-gray-800"><?=$title ?></h1>
    </div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="./">Bahan Baku</a></li>
      <li class="breadcrumb-item active">Bunching</li>
    </ol>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="card">
      <div class="card-body d-flex">
        <div class="form-inline d-inline-flex mr-auto">
          <labe>Category
          <select disabled class="ml-2 form-control custom-select">
            <option>CU</option>
          </select>
        </labe></div>
        <!-- <div class="btn-group">
          <a href="<?=site_url('administrador/order-stok/submit/cu') ?>" class="btn btn-outline-primary active">Transaksi Baru</a>
          <a href="<?=site_url('administrador/material-stok') ?>" class="btn btn-outline-primary">Laporan Summary</a>
        </div> -->
      </div>
      </div>
    </div> 
  </div> 

  <div class="row mt-3 mb-3">

    <aside class="col-md-3">
      <!--   SIDEBAR   -->
      <ul class="list-group mb-3 d-flex justify-content-between">
        <a class="list-group-item" href="<?=site_url('administrador/bahan-baku') ?>"><i class="fa fa-warehouse"></i> Gudang </a>
        <a class="list-group-item" href="<?=site_url('administrador/bahan-baku/drawing') ?>"><i class="fa fa-sync"></i> Drawing </a>
      </ul>

      <a class="btn btn-outline-light btn-block" href="<?=site_url('administrador/bahan-baku/oven-drum') ?>"> 
        <i class="fa fa-table"></i> <span class="text">Oven Drum</span> 
      </a> 

      <!-- <a href="" class="btn btn-outline-light btn-block">
        <div class="d-flex align-items-center">
          <strong>Loading...</strong>
          <div class="spinner-border spinner-border-sm ml-auto"></div>
        </div>
      </a> -->

      <ul class="list-group mt-3 d-flex justify-content-between">
        <a class="list-group-item active" href="<?=site_url('administrador/bahan-baku/bunching') ?>"><i class="fa fa-fill"></i> Bunching </a>
      </ul>
      <!--   SIDEBAR .//END   -->
    </aside>


    <main class="col-md-9">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">SPK Buncher</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Bunching</a>
        </li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header border-bottom d-flex justify-content-between">
                  <p><small>PT. SUKSES SETIA <br> 
                  Jln. Kasir II No. 12 A Desa Pasir Jaya <br> 
                  Jati Uwung - Tangerang</small></p>
                  <div>
                    <p><strong>No. Dokumen</strong> (FR.OP - 0308)</p>
                    <h6><strong>Tanggal </strong> <br> 12 Juli 2020</h6>
                  </div>
                </div>

                <div class="card-body">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                      <div class="row">
                        <label for="no_order" class="col-sm-2 col-form-label">Pilih SPK</label>
                        <div class="col-sm-10">
                          <select class="form-control select-spk">
                            <option>CU</option>
                            <option>PVC</option>
                          </select>
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="no_order" class="col-sm-2 col-form-label">Nomor Order</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="no_order_buncher">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="no_ordercu" class="col-sm-2 col-form-label">Order CU</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="no_ordercu">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="no_orderpvc" class="col-sm-2 col-form-label">Order PVC</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="no_orderpvc">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="no_mesin" class="col-sm-2 col-form-label">No. Mesin</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="no_mesin">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <div class="col-sm-2">Proses</div>
                        <div class="col-sm-10">
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="type_spk">
                            <label class="form-check-label" for="type_spk">
                              Bunching
                            </label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="jenis_ukuran" class="col-sm-2 col-form-label">Jenis / Ukuran</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="jenis_ukuran" value="Auto KTO (1x13 / 0.26)">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="panjang" class="col-sm-2 col-form-label">Panjang</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="panjang" value="64.000 MTR">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="warna" class="col-sm-2 col-form-label">Warna</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="warna">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="panjang_warna" class="col-sm-2 col-form-label">Panjang Per Warna</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="panjang_warna" value="2 Bobin x 12.000 M, 4 Bobin x 10.000 M">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="coilling" class="col-sm-2 col-form-label">Coilling</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="coilling">
                        </div>
                      </div>
                    </li>
                    
                  </ul>
                </div>
                
              </div>
            </div>

            <div class="col-sm-12 mt-3">
              <div class="card">
                <div class="card-header border-bottom text-center text-uppercase">
                  <h4 class="font-weight-bold">Diameter</h4>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-6">
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                          <h5 class="card-title">Filler</h5>
                          <h6 class="card-subtitle mb-2 text-muted">
                            <input type="text" name="" size="4" value=""> mm
                          </h6>
                          <a class="card-link">Nippel: <input type="text" name="" size="4" value=""> mm</a>
                          <a class="card-link">Dies: <input type="text" name="" size="4" value=""> mm</a>
                        </li>
                        <li class="list-group-item">
                          <h5 class="card-title">Sheating</h5>
                          <h6 class="card-subtitle mb-2 text-muted">
                            <input type="text" name="" size="4" value="10.4"> mm
                          </h6>
                          <a class="card-link">Nippel: <input type="text" name="" size="4" value="8.8"> mm</a>
                          <a class="card-link">Dies: <input type="text" name="" size="4" value="16.6"> mm</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                          <h5 class="card-title">Bunch/Strd</h5>
                          <h6 class="card-subtitle mb-2 text-muted">
                            <input type="text" name="" size="4" value="10.4"> mm
                          </h6>
                          <a class="card-link">Dies: <input type="text" name="" size="4" value=""> mm</a>
                        </li>
                        <li class="list-group-item">
                          <h5 class="card-title">Isolasi</h5>
                          <h6 class="card-subtitle mb-2 text-muted">
                            <input type="text" name="" size="4" value=""> mm
                          </h6>
                          <a class="card-link">Nippel: <input type="text" name="" size="4" value=""> mm</a>
                          <a class="card-link">Dies: <input type="text" name="" size="4" value=""> mm</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12 mt-3 mb-5">
              <div class="card">
                <div class="card-body">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                      <div class="row">
                        <label for="pemakaian_pvc" class="col-sm-2 col-form-label">Pemakaian PVC</label>
                        <div class="col-sm-10">
                          <div class="input-group">
                            
                            <input type="text" class="form-control" id="pemakaian_pvc">
                            <div class="input-group-prepend">
                              <div class="input-group-text">Kg</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="jenis_pvc" class="col-sm-2 col-form-label">Jenis PVC</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="jenis_pvc">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="pemakaian_cu" class="col-sm-2 col-form-label">Pemakaian CU</label>
                        <div class="col-sm-10">
                          
                          <div class="input-group">
                            
                            <input type="text" class="form-control" id="pemakaian_cu" value="394">
                            <div class="input-group-prepend">
                              <div class="input-group-text">Kg</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="jumlah_ukuran" class="col-sm-2 col-form-label">Jumlah / Ukuran</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="jumlah_ukuran" value="13 / 0.26">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="printing" class="col-sm-2 col-form-label">Printing</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="printing">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <label for="nama_operator" class="col-sm-2 col-form-label">Nama Operator</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="nama_operator" value="SARDI">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="form-group row">
                        <div class="col-sm-10">
                          <button type="submit" class="btn btn-primary">Kirim SPK Buncher</button>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
          <div class="card">
            <div class="alert-secondary text-white p-1"><?=$this->session->flashdata('message') ?></div>
            <div class="card-header d-flex justify-content-between">
              <div>
                <h4 class="card-title mb-0">Bunching <small>(Barang Setengah Jadi)</small></h4>
                <p>Proses Bobin Setelah Oven Drum</p>
              </div>
              <div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text bg-muted"> Total Stok</span>
                  </div>
                  <input type="text" value="617.575 Kg" size="8" readonly class="form-control" id="result_stok" name="result_stok">
                </div>
              </div>
            </div>
            <div class="card-body">
              
              <form action="" method="POST">
                <div class="form-row">
                  <div class="col-sm-2 form-group">
                    <label for="no_order" class="text-primary">No Order</label>
                    <input type="text" readonly id="no_order" name="no_order" class="form-control" value="BU<?=random(4) ?>">
                  </div> <!-- form-group end.// -->

                  <div class="col form-group">
                    <label for="tgl_order" class="text-primary">Tanggal Order</label>
                    <input type="text" class="form-control" id="datepicker" placeholder="YY-MM-DD HH:MM" name="tgl_order">
                    <?=form_error('tgl_order', '<small class="text-danger">', '</small>') ?>
                  </div> <!-- form-group end.// -->
                </div> <!-- form-row.// -->

                <div class="row">
                  <div class="col-sm-8">
                    <div class="form-group">
                      <div class="form-text text-muted"><small>dari gudang ke proses bunching</small></div>
                      <label for="type_stok" class="text-primary">Laporan untuk barang : </label>
                      <select name="type_stok" disabled id="type_stok">
                        <option value="stok_out">Keluar</option>
                      </select>
                      
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-8">
                    <div class="form-group">
                      <label for="material_kawat_stok_id" class="text-primary">Dari Gudang : </label>
                      <select class="form-control custom-select" id="material_kawat_stok_id" 
                        name="material_kawat_stok_id">
                        <option value="">Select</option>
                        <?php foreach($type_bahanbaku as $tb) : ?>
                        <option value="<?=$tb['id'] ?>" 
                        <?= set_select('material_name', $tb['id'], FALSE) ?>><?=$tb['material_name'] ?></option>
                        <?php endforeach ?>
                      </select>
                      <div class="invalid-feedback">silahkan pilih type material bobinnya.</div>
                      <?=form_error('material_kawat_stok_id', '<small class="text-danger">', '</small>') ?>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label for="result_bobin" class="text-primary">Total Bobin : </label>
                      <input type="text" readonly value="<?=set_value('result_bobin') ?>" placeholder="Total Bobin" class="form-control" id="result_bobin" name="result_bobin">
                      <?=form_error('result_bobin', '<small class="text-danger">', '</small>') ?>
                    </div> 
                  </div>
                </div>

                <div class="form-group">
                  <label for="bobin_besar" class="text-primary">Pilih Bobin Besar : </label>
                  <div class="input-group">
                    <!-- <input type="text" readonly value="<?=set_value('bobin_besar') ?>" placeholder="Nomor Bobin" class="form-control" id="bobin_besar" name="bobin_besar"> -->
                    <select multiple readonly class="form-control" name="bobin_besar[]" id="bobin_besar">
                      <option></option>
                    </select>
                    <input type="hidden" name="material_kawat_id" id="material_kawat_id">
                    <div class="input-group-append">
                      <button disabled class="btn btn-secondary" type="button" id="find-bobin"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                  <?=form_error('bobin_besar', '<small class="text-danger">', '</small>') ?>
                </div>

                <div class="form-group">
                  <label for="material_kawat_stok_id" class="text-primary">Bunching ke : </label>
                  <div class="input-group">
                    <!-- <input type="text" id="size" 
                    name="size" class="form-control" value="<?=set_value('size') ?>"> -->
                    <select id="size" class="js-example-basic-multiple form-control" name="size">
                      <option>0.5</option>
                      <option>0.2</option>
                    </select>

                    <div class="input-group-prepend">
                      <div class="input-group-text">MM2</div>
                      <div class="input-group-text" id="text-supplier"></div>
                    </div>
                  </div>
                  <div class="form-text">
                  <small class="text-muted">ukuran diameter gulungan bobin</small></div>
                  <?=form_error('size', '<small class="text-danger">', '</small>') ?>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label for="barang_keluar" class="text-primary">Total</label>
                      <div class="input-group">
                        <input type="text" id="barang_keluar" 
                        name="barang_keluar" placeholder="Ex: 19.400 - (kg)" class="form-control" 
                        value="<?=set_value('barang_keluar') ?>">
                        <div class="input-group-prepend">
                          <div class="input-group-text">Kg</div>
                        </div>
                      </div>
                      <?=form_error('barang_keluar', '<small class="text-danger">', '</small>') ?>
                      <div class="form-text text-muted"><small>barang keluar untuk di bunching</small></div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="information" class="text-primary">Keterangan</label>
                  <textarea style="resize: none" name="information" cols="50" rows="3" id="information" class="form-control"><?=set_value('information') ?></textarea>
                  <div class="form-text text-muted"><small>Maximal 160 Karakter</small></div>
                  <?=form_error('information', '<small class="text-danger">', '</small>') ?>
                </div>


                <button type="submit" class="btn btn-primary btn-block">Submit</button>
              </form>
            </div> <!-- card-body.// -->
          </div>
        </div>
      </div>

      
    </main>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-bobin">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Data Bobin</h5>
        <button type="button" class="close" data-dismiss="modal">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="<?=base_url('assets') ?>/backend/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url('assets') ?>/backend/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script>
  $(document).ready(function() {

    $('.js-example-basic-multiple').select2({
      theme: 'bootstrap4',
      tags: true,
      tokenSeparators: [',', ' ']
    })

    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'yyyy-mm-dd',
      footer: true, 
      modal: true
    })

    $('#result_stok').val('')

    $('#material_kawat_stok_id').on('change', function() {
      const id = $(this).val()
      if(id) {
        $(this).removeClass('is-invalid')

        $.ajax({
          url: "<?=site_url('administrador/bahan-baku/ajax-laporan-material') ?>",
          method: 'POST',
          data: {id: id},
          success: function(response) {
            const data = JSON.parse(response)
            $('#result_stok').val(data.material_kawat_stok.stok + ' kg')
            $('#text-supplier').text(data.material_kawat_stok.kode_supplier)
            $('#result_bobin').val(data.material_kawat_stok.total_bobin)
            $('#find-bobin').prop('disabled', false)
          }
        })
      } else {
        $(this).addClass('is-invalid')
        $('#result_stok').val('')
        $('#text-supplier').text('')
        $('#result_bobin').val('')
        $('#find-bobin').prop('disabled', true)
      }
    })

    let total = 0;
    let multiple_bobin = []
    let data_items = []

    /*tombol find bobin*/
    $('#find-bobin').on('click', function(e) {
      e.preventDefault()

      $("#bobin_besar").html('')
      $('#barang_keluar').val(0)
      total = 0
      multiple_bobin = []
      data_items = []
        
      let material_kawat_stok_id = $('#material_kawat_stok_id').val()
      $('#modal-bobin .modal-title').text('Data Bobin')

      /*ajax bobin data to database*/
      $.ajax({
        url : '<?= site_url('administrador/bahan-baku/bobin/')  ?>' +material_kawat_stok_id,
        type: "GET",
        dataType: "json",
        success: function(response) {
          if(response.success === true) {
            let html = ''

            /*show modal dialognya*/
            $('#modal-bobin').modal('show')

            /*render elemen table*/
            html += `<table class="table table-striped table-sm" id="table-bobin">
            <thead>
              <tr>
                <th scope="col">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="select_all">
                    <label style="cursor:pointer" class="custom-control-label" for="select_all">Select All</label>
                  </div>
                </th>
                <th scope="col" width="150">ID Bobin</th>
                <th scope="col">Netto</th>
              </tr>
            </thead>
            <tbody>`

            /*dan foreach data pencariannya*/
            if(response.data.length > 0) {
              response.data.forEach((bobin, index) => {
                html += `<tr class="select-bobin" data-bobin="${bobin.id}">
                  <td>
                    <div class="custom-control custom-checkbox small">
                      <input type="checkbox" name="no_bobin[]" 
                      class="custom-control-input check-bobin" id="bobin-${bobin.id}" value="${bobin.no_bobin}">
                      <label class="custom-control-label" for="bobin-${bobin.id}"></label>
                    </div>
                  </td>
                  <td>${bobin.no_bobin}</td>
                  <td>${bobin.netto}</td>
                </tr>`
              })
            }

            if(response.data.length > 0) {
              html += `</tbody>
              <tfoot>
                <tr>
                  <th></th>
                  <th>ID Bobin</th>
                  <th>Netto</th>
                </tr>
              </tfoot>
              </html>`
            } else {
              html += `</tbody>
                </html>`
            }

            $('#modal-bobin .modal-body').html(html)
            $('.select-bobin').css('cursor', 'pointer')

            if(response.data.length > 0) {
              $('#table-bobin tfoot th').each(function () {
                var title = $('#table-bobin tfoot th').eq( $(this).index() ).text()
                if(title) {
                  $(this).html('<input type="text" class="form-control" placeholder="Search '+title+'" />')
                }
              })
            }

            tableBobin = $('#table-bobin').DataTable({
              initComplete: function () {
                $('#table-bobin_filter').hide()

                if(response.data.length > 0) {
                  var row = $('#table-bobin tfoot tr')
                  row.find('th').each(function() {
                    $(this).css('padding', 8)
                  })

                  $('#table-bobin thead').append(row);

                  // Apply the search
                  this.api().columns().every(function () {
                    var col_var = this;

                    $('input', this.footer()).on('keyup change clear', function () {
                      if (col_var.search() !== this.value) {
                        col_var.search(this.value).draw()
                      }
                    })
                  })
                } else {
                  $('#table-bobin_length').hide()
                }
              }
            })

            // handle click on "select all" control
            $('#select_all').on('click', function() {
              // Get all rows with search applied
              var rows = tableBobin.rows({ 'search': 'applied' }).nodes()

              // Check/uncheck checkboxes for all rows in the table
              $('input.check-bobin[type="checkbox"]', rows).prop('checked', this.checked)
              if(this.checked) {
                tableBobin.$('.select-bobin').addClass('table-warning')
                let items = tableBobin.rows('.table-warning').data()

                for(let i = 0; i < items.length; i++) {
                  total += parseFloat(items[i][2])
                  data_items.push(items[i][1] + ' - ' +items[i][2])
                }

                $('#barang_keluar').val(total)
                data_items.forEach(option => {
                  document.getElementById('bobin_besar').innerHTML += `<option selected>${option}</option>` 
                })
              } else {
                tableBobin.$('.select-bobin').removeClass('table-warning')
                data_items = []
                $('#barang_keluar').val(0)
                document.getElementById('bobin_besar').innerHTML = ''
              }
            })
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          Swal.fire({
            icon: 'error',
            title: 'failed',
            text: 'error finding data'
          })
        }
      })
    })

    $('#modal-bobin').on('click', '.select-bobin', function() {
      let material_kawat_id = $(this).data('bobin')
      let item = tableBobin.row($(this)).data()

      if ($(this).hasClass('table-warning')) {
        $(this).removeClass('table-warning')
        $(`#bobin-${material_kawat_id}`).prop('checked', false)
        total -= parseFloat(item[2])
        
      } else {
        $(this).addClass('table-warning')
        $(`#bobin-${material_kawat_id}`).prop('checked', true)
        total += parseFloat(item[2])        
      }

      $('#barang_keluar').val(total)

      let that = $(this)
      /*ajax select data to database*/
      $.ajax({
        url : '<?= site_url('administrador/bahan-baku/select-bobin/')  ?>' +material_kawat_id,
        type: "GET",
        dataType: "json",
        success: function(response) {
          if(response.success === true) {
            var el = $("#bobin_besar")
            el.append(`<option selected>${response.data.no_bobin} - ${response.data.netto}</option>`)

            if (that.hasClass('table-warning')) {
              if(!multiple_bobin.includes(response.data.id)) {
                multiple_bobin.push(response.data.id)
              }
            } else {
              
              const index = multiple_bobin.indexOf(response.data.id)
              if (index > -1) {
                multiple_bobin.splice(index, 1)
              }
            }

            let pval = multiple_bobin.join(", ")
            $("#material_kawat_id").val(pval)
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          Swal.fire({
            icon: 'error',
            title: 'failed',
            text: 'error finding data'
          })
        }
      })
    })
  })
</script>