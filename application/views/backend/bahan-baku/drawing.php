<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" />
<style>
  .input-group-prepend span {
    background-color: #fff;
    border: 1px solid #ced4da;
    color: #495057;
    box-shadow: none !important
  }
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.min.css">
<link href="<?=base_url('assets') ?>/backend/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-2">
    <div class="heading">
      <h1 class="h3 mb-0 text-gray-800"><?=$title ?></h1>
    </div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="./">Bahan Baku</a></li>
      <li class="breadcrumb-item active">Drawing</li>
    </ol>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body d-flex">
          <div class="form-inline d-inline-flex mr-auto">
            <label>Category
              <select disabled class="ml-2 form-control custom-select">
                <option>CU</option>
              </select>
            </label>
          </div>
        </div>
      </div>
    </div> 
  </div> 

  <div class="row mt-3 mb-3">

    <aside class="col-md-3 mb-3">
      <!--   SIDEBAR   -->
      <ul class="list-group mb-3 d-flex justify-content-between">
        <a class="list-group-item" href="<?=site_url('administrador/bahan-baku') ?>"><i class="fa fa-warehouse"></i> Gudang </a>
        <a class="list-group-item active" href="<?=site_url('administrador/bahan-baku/drawing') ?>"><i class="fa fa-chevron-right"></i> Drawing </a>
        <!-- <a class="list-group-item" href="<?=site_url('administrador/oven-drum/submit') ?>"><i class="fa fa-sync"></i> Submit Drawing </a> -->
      </ul>

      <a class="btn btn-outline-light btn-block" href="<?=site_url('administrador/bahan-baku/oven-drum') ?>"> 
        <i class="fa fa-table"></i> <span class="text">Oven Drum</span> 
      </a> 

      <!-- <a href="" class="btn btn-outline-light btn-block">
        <div class="d-flex align-items-center">
          <strong>Loading...</strong>
          <div class="spinner-border spinner-border-sm ml-auto"></div>
        </div>
      </a> -->

      <ul class="list-group mt-3 d-flex justify-content-between">
        <a class="list-group-item" href="<?=site_url('administrador/bahan-baku/bunching') ?>"><i class="fa fa-fill"></i> Bunching </a>
      </ul>
      <!--   SIDEBAR .//END   -->
    </aside>


    <main class="col-md-9">
      <div class="card">
        <div class="alert-warning text-white p-1"><?=$this->session->flashdata('message') ?></div>

        <div class="card-header d-flex justify-content-between">
          <div>
            <h4 class="card-title mb-0">Drawing</h4>
            <p>Proses bobin besar ke bobin kecil</p>
          </div>
          <div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text bg-muted">Total Stok</span>
              </div>
              <input type="text" size="8" readonly class="form-control" id="result_stok" name="result_stok">
            </div>
          </div>
        </div>
        <div class="card-body">
          
          <form action="<?=site_url('administrador/bahan-baku/drawing') ?>" method="POST">
            <div class="form-row">
              <div class="col-sm-2 form-group">
                <label for="no_order" class="text-primary">No Order</label>
                <input type="text" readonly id="no_order" name="no_order" class="form-control" 
                value="D<?=random(4) ?>">
              </div> <!-- form-group end.// -->

              <div class="col form-group">
                <label for="tgl_order" class="text-primary">Tanggal Order</label>
                <input type="text" class="form-control" id="datepicker" placeholder="YY-MM-DD HH:MM" name="tgl_order">
                <?=form_error('tgl_order', '<small class="text-danger">', '</small>') ?>
              </div> <!-- form-group end.// -->
            </div> <!-- form-row.// -->

            <div class="row">
              <div class="col-sm-8">
                <div class="form-group">
                  <div class="form-text text-muted mb-1"><small>proses drawing dari gudang</small></div>
                  <label for="type_stok" class="text-primary">Laporan untuk barang : </label>
                  <select name="type_stok" disabled id="type_stok">
                    <option value="stok_out">Keluar</option>
                  </select>
                  
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-8">
                <div class="form-group">
                  <label for="material_kawat_stok_id" class="text-primary">Dari Gudang : </label>
                  <select class="form-control custom-select" id="material_kawat_stok_id" 
                    name="material_kawat_stok_id">
                    <option value="">Select</option>
                    <?php foreach($type_bahanbaku as $tb) : ?>
                    <option value="<?=$tb['id'] ?>" 
                    <?= set_select('material_name', $tb['id'], FALSE) ?>><?=$tb['material_name'] ?></option>
                    <?php endforeach ?>
                  </select>
                  <div class="invalid-feedback">silahkan pilih type material bobinnya.</div>
                  <?=form_error('material_kawat_stok_id', '<small class="text-danger">', '</small>') ?>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="result_bobin" class="text-primary">Total Bobin : </label>
                  <input type="text" readonly value="<?=set_value('result_bobin') ?>" placeholder="Total Bobin" class="form-control" id="result_bobin" name="result_bobin">
                  <?=form_error('result_bobin', '<small class="text-danger">', '</small>') ?>
                </div> 
              </div>
            </div>

            <div class="form-group">
              <label for="bobin_besar" class="text-primary">Pilih Bobin Besar : </label>
              <div class="input-group">
                <input type="text" readonly value="<?=set_value('bobin_besar') ?>" placeholder="Nomor Bobin" class="form-control" id="bobin_besar" name="bobin_besar">
                <input type="hidden" name="material_kawat_id" id="material_kawat_id">
                <input type="hidden" name="no_bobin" id="no_bobin">
                <div class="input-group-append">
                  <button disabled class="btn btn-secondary" type="button" id="find-bobin"><i class="fa fa-search"></i></button>
                </div>
              </div>
              <?=form_error('bobin_besar', '<small class="text-danger">', '</small>') ?>
            </div>

            <div class="form-group">
              <label for="material_kawat_stok_id" class="text-primary">Ditarik Menjadi Bobin Kecil : </label>
              <div class="input-group">
                <!-- <input type="text" id="size" 
                name="size" class="form-control" value="<?=set_value('size') ?>"> -->
                <select id="size" class="js-example-basic-multiple form-control" name="size">
                  <option>0.26</option>
                  <option>0.18</option>
                </select>

                <div class="input-group-prepend">
                  <div class="input-group-text">MM</div>
                  <div class="input-group-text" id="text-supplier"></div>
                </div>
              </div>
              <div class="form-text">
              <small class="text-muted">ukuran diameter gulungan bobin</small></div>
              <?=form_error('size', '<small class="text-danger">', '</small>') ?>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="barang_keluar" class="text-primary">Total</label>
                  <div class="input-group">
                    <input type="text" id="barang_keluar" 
                    name="barang_keluar" readonly placeholder="Ex: 19.400 - (kg)" class="form-control" 
                    value="<?=set_value('barang_keluar') ?>">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Kg</div>
                    </div>
                  </div>
                  <?=form_error('barang_keluar', '<small class="text-danger">', '</small>') ?>
                  <div class="form-text text-muted"><small>barang keluar untuk di drawing</small></div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="text-primary" for="drawing">Nomor Drawing</label>
              <select id="drawing" class="drawing form-control" name="drawing">
                <option>Drawing 1</option>
                <option>Drawing 2</option>
                <option>Drawing 3</option>
                <option>Drawing 4</option>
                <option>Drawing 5</option>
                <option>Drawing 6</option>
                <option>Drawing 7</option>
              </select>
            </div>

            <div class="form-group">
              <label for="information" class="text-primary">Keterangan</label>
              <textarea style="resize: none" name="information" cols="50" rows="3" id="information" class="form-control"><?=set_value('information') ?></textarea>
              <div class="form-text text-muted"><small>Maximal 160 Karakter</small></div>
              <?=form_error('information', '<small class="text-danger">', '</small>') ?>
            </div>


            <button type="submit" class="btn btn-primary btn-block">Submit</button>
          </form>
        </div> <!-- card-body.// -->
      </div>
    </main>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-bobin">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Data Bobin</h5>
        <button type="button" class="close" data-dismiss="modal">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="<?=base_url('assets') ?>/backend/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url('assets') ?>/backend/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script>
  $(document).ready(function() {

    $('.drawing').select2({
      theme: 'bootstrap4'
    })

    $('.js-example-basic-multiple').select2({
      theme: 'bootstrap4',
      tags: true,
      tokenSeparators: [',', ' ']
    })

    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'yyyy-mm-dd',
      footer: true, 
      modal: true
    })

    $('#result_stok').val('')

    $('#material_kawat_stok_id').on('change', function() {
      const id = $(this).val()
      if(id) {
        $(this).removeClass('is-invalid')

        $.ajax({
          url: "<?=site_url('administrador/bahan-baku/ajax-laporan-material') ?>",
          method: 'POST',
          data: {id: id},
          success: function(response) {
            const data = JSON.parse(response)
            $('#result_stok').val(data.material_kawat_stok.stok + ' kg')
            $('#text-supplier').text(data.material_kawat_stok.kode_supplier)
            $('#result_bobin').val(data.material_kawat_stok.total_bobin)
            $('#find-bobin').prop('disabled', false)
          }
        })
      } else {
        $(this).addClass('is-invalid')
        $('#result_stok').val('')
        $('#text-supplier').text('')
        $('#result_bobin').val('')
        $('#find-bobin').prop('disabled', true)
      }
    })

    /*tombol find bobin*/
    $('#find-bobin').on('click', function(e) {
      e.preventDefault()
        
      let material_kawat_stok_id = $('#material_kawat_stok_id').val()
      $('#modal-bobin .modal-title').text('Data Bobin')

      /*ajax bobin data to database*/
      $.ajax({
        url : '<?= site_url('administrador/bahan-baku/bobin/')  ?>' +material_kawat_stok_id,
        type: "GET",
        dataType: "json",
        success: function(response) {
          if(response.success === true) {
            let html = ''

            /*show modal dialognya*/
            $('#modal-bobin').modal('show')

            /*render elemen table*/
            html += `<table class="table table-striped table-sm" id="table-bobin">
            <thead>
              <tr>
                <th scope="col" width="50">No</th>
                <th scope="col" width="150">ID Bobin</th>
                <th scope="col" width="150">Berat</th>
                <th scope="col" width="150">Bruto</th>
                <th scope="col">Netto</th>
              </tr>
            </thead>
            <tbody>`

            /*dan foreach data pencariannya*/
            if(response.data.length > 0) {
              response.data.forEach((bobin, index) => {
                html += `<tr class="select-bobin" data-bobin="${bobin.id}">
                  <th scope="row">${index+1}</th>
                  <td>${bobin.no_bobin}</td>
                  <td>${bobin.berat_bobin}</td>
                  <td>${bobin.bruto}</td>
                  <td>${bobin.netto}</td>
                </tr>`
              })
            }

            if(response.data.length > 0) {
              html += `</tbody>
              <tfoot>
                <tr>
                  <th></th>
                  <th>ID Bobin</th>
                  <th>Berat</th>
                  <th>Bruto</th>
                  <th>Netto</th>
                </tr>
              </tfoot>
              </html>`
            } else {
              html += `</tbody>
                </html>`
            }

            $('#modal-bobin .modal-body').html(html)
            $('.select-bobin').css('cursor', 'pointer')

            if(response.data.length > 0) {
              $('#table-bobin tfoot th').each(function () {
                var title = $('#table-bobin tfoot th').eq( $(this).index() ).text()
                if(title) {
                  $(this).html('<input type="text" class="form-control" placeholder="Search '+title+'" />')
                }
              })
            }

            tableBobin = $('#table-bobin').DataTable({
              initComplete: function () {
                $('#table-bobin_filter').hide()

                if(response.data.length > 0) {
                  var row = $('#table-bobin tfoot tr')
                  row.find('th').each(function() {
                    $(this).css('padding', 8)
                  })

                  $('#table-bobin thead').append(row);

                  // Apply the search
                  this.api().columns().every(function () {
                    var col_var = this;

                    $('input', this.footer()).on('keyup change clear', function () {
                      if (col_var.search() !== this.value) {
                        col_var.search(this.value).draw()
                      }
                    })
                  })
                } else {
                  $('#table-bobin_length').hide()
                }
              }
            })
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          alert('failed')
        }
      })
    })

    $('#modal-bobin').on('click', '.select-bobin', function() {
      let material_kawat_id = $(this).data('bobin')
      $(this).addClass('table-warning')
      tableBobin.$(".select-bobin").not($(this)).removeClass('table-warning')

      /*ajax select data to database*/
      $.ajax({
        url : '<?= site_url('administrador/bahan-baku/select-bobin/')  ?>' +material_kawat_id,
        type: "GET",
        dataType: "json",
        success: function(response) {
          if(response.success === true) {
            var el = $("#bobin_besar")
            el.val(`${response.data.no_bobin} - ${response.data.netto}`)
            $("#material_kawat_id").val(response.data.id)
            $("#no_bobin").val(response.data.no_bobin)
            $("#barang_keluar").val(response.data.netto)
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          Swal.fire({
            icon: 'error',
            title: 'Failed',
            text: 'Error finding data'
          })
        }
      })
    })
  })
</script>