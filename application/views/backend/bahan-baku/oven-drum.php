<link href="<?=base_url('assets') ?>/backend/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" />
<style>
  .input-group-append .text-shadow-none, .input-group-prepend .text-shadow-none {
  	box-shadow: none !important;
  	border: 1px solid #ced4da
  }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-2">
    <div class="heading">
      <h1 class="h3 mb-0 text-gray-800"><?=$title ?></h1>
    </div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="./">Drawing</a></li>
      <li class="breadcrumb-item active">Oven Drum</li>
    </ol>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body d-flex">
          <div class="form-inline d-inline-flex mr-auto">
            <label>Category
              <select disabled class="ml-2 form-control custom-select">
                <option>CU</option>
              </select>
            </label>
          </div>
          <div class="btn-group">
            <?php foreach($type_mesin as $oven) : ?>
              <a href="<?=strtolower($oven->no_machine) ?>" data-info="<?=$oven->type_machine ?>" class="btn btn-outline-primary select-oven"><?=$oven->type_machine ?> 

              <?php $this->db->select('id'); 
              $drum = $this->db->get_where('material_oven_drum', ['no_machine' => $oven->no_machine, 'status' => 0]) ?>
              <span class="badge badge-danger"><?=$drum->num_rows() ?></span></a>
            <?php endforeach ?>
          </div>
        </div>
      </div>
    </div> 
  </div> 

  <div class="row mt-3 mb-3">

    <aside class="col-md-3 mb-3">
      <!--   SIDEBAR   -->
      <ul class="list-group mb-3 d-flex justify-content-between">
        <a class="list-group-item" href="<?=site_url('administrador/bahan-baku') ?>"><i class="fa fa-warehouse"></i> Gudang </a>
        <a class="list-group-item" href="<?=site_url('administrador/bahan-baku/drawing') ?>"><i class="fa fa-chevron-right"></i> Drawing </a>
        <!-- <a class="list-group-item" href="<?=site_url('administrador/oven-drum/submit') ?>"><i class="fa fa-sync"></i> Submit Drawing </a> -->
      </ul>

      <a class="btn btn-outline-light btn-block active" href="<?=site_url('administrador/bahan-baku/oven-drum') ?>"> 
        <i class="fa fa-table"></i> <span class="text">Oven Drum</span> 
      </a> 

      <!-- <a href="" class="btn btn-outline-light btn-block">
        <div class="d-flex align-items-center">
          <strong>Loading...</strong>
          <div class="spinner-border spinner-border-sm ml-auto"></div>
        </div>
      </a> -->

      <ul class="list-group mt-3 d-flex justify-content-between">
        <a class="list-group-item" href="<?=site_url('administrador/bahan-baku/bunching') ?>"><i class="fa fa-fill"></i> Bunching </a>
      </ul>
      <!--   SIDEBAR .//END   -->
    </aside>

    <main class="col-md-9">

			<div class="card">
        <div class="alert-info text-white p-1"><?=$this->session->flashdata('message') ?></div>
				<div class="card-body">
					<header class="mb-3 d-flex justify-content-between">
            <div>
						  <h4 class="card-title mb-0">Oven Drum 1/2/3</h4>
              <p class="icontext"><i class="icon text-muted fa fa-spinner"></i> Proses pemanasan bobin sekitar 1-3 hari</p>
            </div>
						<div>
							<a href="<?=site_url('administrador/oven-drum/submit') ?>" 
							class="btn btn-sm btn-outline-primary">Submit Oven <i class="fa fa-chevron-right"></i></a>
						</div>
					</header>

  				<table class="table table-borderless" id="table-bobin">
  					<thead class="text-muted thead-light">
  						<tr class="small text-uppercase">
                <th scope="col" width="20">No</th>
                <th scope="col" width="200">Drawing</th>

                <th scope="col" width="100">No Bobin</th>
  						  <th scope="col" width="130">Netto <small>(kg)</small></th>
  						  <th scope="col" class="text-right" width="140"> </th>
  						</tr>
  					</thead>
  					<tbody>
  						<!-- <tr>
  							<td>05.03.20</td>
  							<td>18.5</td>
  							<td>81.4</td>
  								
  							<td>62.9</td>
  							<td class="text-right"> 
  								<a href="" class="btn btn-outline-light"> <i class="fa fa-trash"></i></a>
  							</td>
  						</tr> -->
            </tbody>
            <tbody>
  						<tr>
  							<td colspan="3" class="text-right">Total</td>
  							<td colspan="2">- Kg</td>
  						</tr>
  					</tbody>
  				</table>
        </div>

			</div> <!-- card.// -->


    </main>
  </div>
</div>

<div class="modal" id="lihatBobin">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Detail Bobin</h5>
        <button type="button" class="close" data-dismiss="modal">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="list-group list-group-flush">
          <li class="list-group-item">Cras justo odio</li>
          <li class="list-group-item">Dapibus ac facilisis in</li>
          <li class="list-group-item">Morbi leo risus</li>
          <li class="list-group-item">Porta ac consectetur ac</li>
          <li class="list-group-item">Vestibulum at eros</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!-- Page level plugins -->
<script src="<?=base_url('assets') ?>/backend/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url('assets') ?>/backend/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script>
  $(document).ready(function() {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'yyyy-mm-dd',
      footer: true, 
      modal: true
    })
  })

  // global variable
  var manageOvenTable
  $(document).ready(function() {
    manageOvenTable = $("#table-bobin").DataTable({
      'orders': []
    })

    $('.select-oven').on('click', function(e) {
      e.preventDefault()
      let no_machine = $(this).attr('href')
      let text = $(this).data('info')
      $('.card-title').text(text)
      manageOvenTable.ajax.url('<?php echo site_url('administrador/bahan-baku/ajax-ovendrum/')  ?>' +no_machine).load()
    })  
  })
</script>