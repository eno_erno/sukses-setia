<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Drawing extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Bangkok");
		$this->load->model('Basic_model', 'basic');
	}

	public function index()
	{
		$result = array('data' => array());

		$this->db->select('no_bobin, material_drawing, drawing, id, total_netto');
		$data = $this->db->get('material_drawing')->result_array();
		
		$no = 1;
		foreach ($data as $key => $value) :

			$result['data'][] = array(
				"material_drawing_id" => $value['id'],
				"no_bobin" => $value['no_bobin'],
				"material_drawing_name" => $value['material_drawing'],
				"drawing" => $value['drawing'],
				"total_netto" => $value['total_netto']
			);

			$no++;
		endforeach;

		echo json_encode($result);
	}

	public function result_bobin($material_stok_id, $from_bobin)
	{
		$result = array('data' => array());

		$drawing = $this->db->query("SELECT no_bobin, netto FROM material_oven_drum 
			WHERE material_kawat_stok_id = '$material_stok_id'
			AND from_bobin = '$from_bobin' 
			AND status = 1 ");

		$total_barang_masuk = 0;
		foreach ($drawing->result_array() as $key => $value) :
			$total_barang_masuk += $value['netto'];
		endforeach;

		$result['data'] = array(
			"total_bobin" => $drawing->num_rows(),
			"total_barang_masuk" => $total_barang_masuk
		);

		echo json_encode($result);
	}

}