<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Material_stok extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Bangkok");
		$this->load->model('Basic_model', 'basic');
	}

	public function index()
	{
		$data['title'] = 'Laporan <strong>Bahan Baku</strong>';
		$data['user'] = $this->db->get_where('user', 
			['username' => $this->session->userdata('username')])->row_array();

		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/sidebar', $data);
		$this->load->view('backend/templates/topbar', $data);
		$this->load->view('backend/stok_material/index', $data);
		$this->load->view('backend/templates/footer');
	}

	public function getResultPVC()
	{
		$result = array('data' => array());

		$data = $this->db->get('material_pvc')->result_array();
		$no = 1;
		foreach ($data as $key => $value) :
			$material_pvc = $this->db->get_where('material_pvc_stok', ['material_pvc_id' => $value['id']])->row_array();

			#button action
			$buttons = '
				<div class="btn-group">
					<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"></button>
				  <div class="dropdown-menu">
				    <a class="dropdown-item" 
				    href="'.site_url('administrador/material-stok/submit/pvc/'.strtolower($value['slug'])).'">
				    <i class="fa fa-eye"></i> Lihat Laporan</a>

				    <a class="dropdown-item" href="#"><i class="fa fa-times"></i> Hapus</a>
				  </div>
				</div>
			';

			$checkbox = '<div class="custom-control custom-checkbox small">
        <input type="checkbox" name="pvc_name[]" 
        class="custom-control-input delete-checkbox" id="customCheck-'.$value['id'].'" value="'.$value['id'].'">
        <label class="custom-control-label" for="customCheck-'.$value['id'].'"></label>
      </div>';

			$result['data'][$key] = array(
				'id' => $no,
				'checkbox' => $checkbox,
				'pvc_name' => $value['pvc_name'],
				'stok' => $material_pvc['stok'],
				'action' => $buttons
			);

			$no++;
		endforeach;

		echo json_encode($result);
	}

	public function getResultBunching()
	{
		$result = array('data' => array());

		$this->db->order_by('id', 'DESC');
		$data = $this->db->get('material_kawat_stok')->result_array();
		
		$no = 1;
		foreach ($data as $key => $value) :
			#button action
			$buttons = '
				<div class="btn-group">
					<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"></button>
				  <div class="dropdown-menu">
				  	<a class="dropdown-item" 
				    href="'.site_url('administrador/material-stok/type/cu/'.strtolower($value['slug'])).'">
				    <i class="fa fa-file-signature"></i> Catatan Bobin</a>

				    <a class="dropdown-item" 
				    href="'.site_url('administrador/material-stok/submit/cu/'.strtolower($value['slug'])).'">
				    <i class="fa fa-box"></i> Barang Masuk</a>

				    <a class="dropdown-item" href="#"><i class="fa fa-times"></i> Hapus</a>
				  </div>
				</div>
			';

			$checkbox = '<div class="custom-control custom-checkbox small">
        <input type="checkbox" name="bunching[]" 
        class="custom-control-input delete-checkbox" id="customCheck-'.$value['id'].'" value="'.$value['id'].'">
        <label class="custom-control-label" for="customCheck-'.$value['id'].'"></label>
      </div>';

      $material_kawat = $this->db->get_where('material_kawat', 
      	['material_kawat_stok_id' => $value['id']])->result_array();

			$result['data'][$key] = array(
				'id' => $no,
				'checkbox' => $checkbox,
				'material_name' => $value['material_name'],
				'stok_bobin' => $value['stok'],
				'material_kawat' => $material_kawat,
				'action' => $buttons
			);

			$no++;
		endforeach;

		echo json_encode($result);
	}

	public function getStokBahanBaku($id)
	{
		$result = array('data' => array());

		$data = $this->db->get_where('material_kawat_order', 
			['material_kawat_stok_id' => $id])->result_array();

		$no = 1;
		foreach ($data as $key => $value) :
			#button action
			$buttons = '
				<div class="btn-group">
					<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"></button>
				  <div class="dropdown-menu">
				    <a class="dropdown-item" 
				    href="'.site_url('administrador/material-stok/detail/'.$value['id']).'">Detail</a>
				  </div>
				</div>
			';

			$checkbox = '<div class="custom-control custom-checkbox small">
        <input type="checkbox" name="laporancu[]" 
        class="custom-control-input delete-checkbox" id="customCheck-'.$value['id'].'" value="'.$value['id'].'">
        <label class="custom-control-label" for="customCheck-'.$value['id'].'"></label>
      </div>';

			$result['data'][$key] = array(
				'id' => $no,
				'checkbox' => $checkbox,
				'tgl_order' => tgl_indo($value['tgl_order']),
				'no_order' => $value['no_order'],
				'incoming_stok' => $value['incoming_stok'],
				'stok_out' => $value['stok_out'],
				'result_stok' => $value['result_stok'],
				'information' => $value['information'],
				'action' => $buttons
			);

			$no++;
		endforeach;

		echo json_encode($result);
	}

	public function getStokBahanPVC($id)
	{
		$result = array('data' => array());

		$data = $this->db->get_where('material_pvc_order', 
			['material_pvc_id' => $id])->result_array();

		$no = 1;
		foreach ($data as $key => $value) :
			#button action
			$buttons = '
				<div class="btn-group">
					<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"></button>
				  <div class="dropdown-menu">
				    <a class="dropdown-item" 
				    href="'.site_url('administrador/material-stok/detail-pvc/'.$value['id']).'">Detail</a>
				  </div>
				</div>
			';

			$checkbox = '<div class="custom-control custom-checkbox small">
        <input type="checkbox" name="laporanpvc[]" 
        class="custom-control-input delete-checkbox" id="customCheck-'.$value['id'].'" value="'.$value['id'].'">
        <label class="custom-control-label" for="customCheck-'.$value['id'].'"></label>
      </div>';

			$result['data'][$key] = array(
				'id' => $no,
				'checkbox' => $checkbox,
				'tgl_order' => tgl_indo($value['tgl_order']),
				'no_order' => $value['no_order'],
				'incoming_stok' => $value['incoming_stok'],
				'stok_out' => $value['stok_out'],
				'result_stok' => $value['result_stok'],
				'information' => $value['information'],
				'action' => $buttons
			);

			$no++;
		endforeach;

		echo json_encode($result);
	}

	public function laporan($bahan)
	{
		if($bahan == 'pvc') {

			$data['title'] = 'Laporan <strong>Bahan Baku</strong>';
			$data['user'] = $this->db->get_where('user', 
				['username' => $this->session->userdata('username')])->row_array();

			$this->load->view('backend/templates/header', $data);
			$this->load->view('backend/templates/sidebar', $data);
			$this->load->view('backend/templates/topbar', $data);
			$this->load->view('backend/stok_material/pvc', $data);
			$this->load->view('backend/templates/footer');
		}
	}

	public function type($bahan, $slug)
	{
		if($bahan == 'cu') {
			$data['title'] = 'Bahan <strong>Baku</strong>';
			$data['user'] = $this->db->get_where('user', 
				['username' => $this->session->userdata('username')])->row_array();

			$data['material'] = $this->db->get_where('material_kawat_stok', ['slug' => $slug])->row_array(); 
			$data['material_type'] = $this->db->get('material_type')->result_array(); 
			$data['supplier'] = $this->db->get('supplier')->result_array(); 

			$this->form_validation->set_rules('no_bobin[]', 'No Bobin', 'required');
			$this->form_validation->set_rules('berat_bobin[]', 'Berat Bobin', 'required');
			$this->form_validation->set_rules('bruto[]', 'Bruto', 'required');

			if($this->form_validation->run() === false) :
				$this->load->view('backend/templates/header', $data);
				$this->load->view('backend/templates/sidebar', $data);
				$this->load->view('backend/templates/topbar', $data);
				$this->load->view('backend/stok_material/order', $data);
				$this->load->view('backend/templates/footer');
			else:
				$this->db->select('stok, id');
				$bahanbaku = $this->db->get_where('material_kawat_stok', ['slug' => $slug])->row_array();

				$post = $this->input->post();
				$bobin = $post['no_bobin'];
				$berat_bobin = $post['berat_bobin'];
				$bruto = $post['bruto'];
				
				foreach ($bobin as $key => $value) :
    			$cekNoBobin = $this->db->get_where('material_kawat', 
    				[
    					'no_bobin' => $value,
    					'material_kawat_stok_id' => $bahanbaku['id']
    				])
    			->row();
    			
    			if(!$cekNoBobin) :
						$data = [
							'material_kawat_stok_id' => $bahanbaku['id'],
							'no_bobin' => $value,
							'berat_bobin' => $berat_bobin[$key],
							'bruto' => $bruto[$key],
							'netto' => $bruto[$key]-$berat_bobin[$key]
						];

		    		$this->db->insert('material_kawat', $data);
		      else:
		      	$data = [
							'berat_bobin' => $berat_bobin[$key],
							'bruto' => $bruto[$key],
							'netto' => $bruto[$key]-$berat_bobin[$key]
						];
		      	$this->basic->update('no_bobin', $value, $data, 'material_kawat');
		      endif;
    		endforeach;
				
				$this->session->set_flashdata("message", 
					'<div class="alert alert-success">Bobin kawat sudah berhasil disimpan</div>');
				redirect('administrador/material-stok');
			endif;
		}
	}

	public function submit($bahan, $slug)
	{
		if($bahan == 'cu') {
			$data['title'] = 'Transaksi <strong>Barang Masuk</strong>';
			$data['user'] = $this->db->get_where('user', 
				['username' => $this->session->userdata('username')])->row_array();

			$fullname = $data['user']['fullname'];

			$data['material'] = $this->db->get_where('material_kawat_stok', ['slug' => $slug])->row_array(); 
			$data['material_type'] = $this->db->get('material_type')->result_array(); 
			$data['supplier'] = $this->db->get('supplier')->result_array(); 

			$this->form_validation->set_rules('tgl_order', 'Tgl Order', 'required');
			$this->form_validation->set_rules('barang_masuk', 'Barang Masuk', 'required');
			$this->form_validation->set_rules('information', 'Keterangan', 'required');

			if($this->form_validation->run() === false) :
				$this->load->view('backend/templates/header', $data);
				$this->load->view('backend/templates/sidebar', $data);
				$this->load->view('backend/templates/topbar', $data);
				$this->load->view('backend/stok_material/submit', $data);
				$this->load->view('backend/templates/footer');
			else:
				$post = $this->input->post();
				$position = $post['position'];

				$this->db->select('stok, id, total_bobin');
				$bahanbaku = $this->db->get_where('material_kawat_stok', 
					['slug' => $slug])
				->row_array();

				$stok_in = $post['barang_masuk'];
				$result_stok = $bahanbaku['stok'] + $stok_in; 

				$data_update = [
					'stok' => $result_stok,
					'position' => 'gudang'
				];

				$material_stok_id = $bahanbaku['id'];
				if($position == 'Drawing') {

					$from_bobin = $post['drawing'];

					$queryBobin = $this->db->query("
			    	SELECT id, no_bobin, netto, berat_bobin, bruto
			    	FROM material_oven_drum
			    	WHERE material_kawat_stok_id = '$material_stok_id'
			    	AND from_bobin = '$from_bobin'
			    	AND status = 1
			    	ORDER BY no_bobin ");

		  		$bobins = $queryBobin->result_array();
		  		foreach($bobins as $bobin) : 
		  			$data_bobin = [
							'material_kawat_stok_id' => $material_stok_id,
							'no_bobin' => $bobin['no_bobin'],
							'berat_bobin' => $bobin['berat_bobin'],
							'bruto' => $bobin['bruto'],
							'netto' => $bobin['netto']
						];

						$this->db->insert('material_kawat', $data_bobin);
		  		endforeach;

		  		#delete data bobin di ovendrum
		  		$this->db->query("DELETE FROM material_oven_drum 
		  			WHERE material_kawat_stok_id = '$material_stok_id' AND from_bobin = '$from_bobin' AND status = 1");
		  		$data_update['total_bobin'] = $bahanbaku['total_bobin'] + $post['result_bobin'];
				} else {
					$data_update['total_bobin'] = $bahanbaku['total_bobin'] + $post['result_bobin'];
				}

				$data_insert = [
					'material_kawat_stok_id' => $bahanbaku['id'],
					'no_order' => $post['no_order'],
					'incoming_stok' => $stok_in,
					'stok_out' => '',
					'result_stok' => $result_stok,
					'information' => $post['information'],
					'tgl_order' => $post['tgl_order'],
					'operator' => $fullname
				];

				$this->db->insert('material_kawat_order', $data_insert);
				$this->basic->update('id', $bahanbaku['id'], $data_update, 'material_kawat_stok');
				$bobin_id = $this->db->query("SELECT no_bobin FROM material_kawat WHERE material_kawat_stok_id = '$material_stok_id' ")->result();

				$temp = [];
				foreach ($bobin_id as $bi) :
					array_push($temp, $bi->no_bobin);
				endforeach;
				$max = max($temp);

				for($i = 1; $i <= $post['result_bobin']; $i++) :
					$max++;

					$insert_bobin = [
						'material_kawat_stok_id' => $bahanbaku['id'],
						'no_bobin' => $max,
						'created_at' => $post['tgl_order'] . " " . date('H:i:s')
					];

					$this->db->insert('material_kawat', $insert_bobin);
				endfor;

				$this->session->set_flashdata("message", 
					'<div class="alert alert-success">Order stok kawat masuk berhasil diupdated</div>');
				redirect('administrador/material-stok');
			endif;	

		} else {
			$data['title'] = 'Transaksi <strong>Bahan PVC</strong>';
			$data['user'] = $this->db->get_where('user', 
				['username' => $this->session->userdata('username')])->row_array();

			$data['material'] = $this->db->get_where('material_pvc', ['slug' => $slug])->row_array();
			$data['pvc_stok'] = $this->db->get_where('material_pvc_stok', ['material_pvc_id' => $data['material']['id']])->row_array(); 
			
			$fullname = $data['user']['fullname'];
			$data['colors'] = $this->db->get('color')->result_array(); 
			$data['supplier'] = $this->db->get('supplier')->result_array();  

			$this->form_validation->set_rules('tgl_order', 'Tgl Order', 'required');
			$this->form_validation->set_rules('barang_masuk', 'Barang Masuk', 'required');
			$this->form_validation->set_rules('information', 'Keterangan', 'required');

			if($this->form_validation->run() === false) :
				$this->load->view('backend/templates/header', $data);
				$this->load->view('backend/templates/sidebar', $data);
				$this->load->view('backend/templates/topbar', $data);
				$this->load->view('backend/stok_material/submit-pvc', $data);
				$this->load->view('backend/templates/footer');
			else:

				$this->db->select('id');
				$bahanbaku = $this->db->get_where('material_pvc', ['slug' => $slug])->row_array(); 

				$this->db->select('stok');
				$pvc_stok = $this->db->get_where('material_pvc_stok', 
					['material_pvc_id' => $bahanbaku['id']])
				->row_array();

				$stok_in = $this->input->post('barang_masuk', true);
				$result_stok = $pvc_stok['stok'] + $stok_in; 

				$data = [
					'material_pvc_id' => $bahanbaku['id'],
					'no_order' => $this->input->post('no_order', true),
					'incoming_stok' => $stok_in,
					'stok_out' => '',
					'result_stok' => $result_stok,
					'information' => $this->input->post('information', true),
					'tgl_order' => $this->input->post('tgl_order', true),
					'operator' => $fullname
				];

				$this->db->insert('material_pvc_order', $data);

				$dataUpdate = [
					'stok' => $result_stok
				];

				$this->basic->update('id', $bahanbaku['id'], $dataUpdate, 'material_pvc_stok');
				$this->session->set_flashdata("message", 
					'<div class="alert alert-success">Order stok pvc masuk berhasil diupdated</div>');
				redirect('administrador/material-stok/laporan/pvc');
			endif;
		}
	}

	public function remove_all_laporan_pvc()
	{
		$laporanpvc = $this->input->post('laporanpvc');

		foreach ($laporanpvc as $value) {
		  $this->basic->delete('id', $value, 'material_pvc_order'); 
		}

		echo json_encode([
			'status' => true,
			'message' => 'Beberapa data order kawat berhasil di hapus'	
		]);
	}

	public function remove_all_laporan_cu()
	{
		$laporancu = $this->input->post('laporancu');

		foreach ($laporancu as $value) {
		  $this->basic->delete('id', $value, 'material_kawat_order'); 
		}

		echo json_encode([
			'status' => true,
			'message' => 'Beberapa data order pvc berhasil di hapus'	
		]);
	}

	public function detail()
	{
		# code...
	}

	public function detail_pvc()
	{
		# code...
	}

	public function result_drawing($material_stok_id, $from_bobin)
	{
		$response = array('success' => false, 'messages' => array());

		$queryBobin = $this->db->query("SELECT id, no_bobin, netto, berat_bobin, bruto
	    	FROM material_oven_drum
		    	WHERE material_kawat_stok_id = '$material_stok_id'
		    	AND from_bobin = '$from_bobin'
		    	AND status = 1
	    	ORDER BY no_bobin ");

  	$bobins = $queryBobin->result_array();

    $response['success'] = true;
    $response['messages'] = '';
    $response['data'] = $bobins;

    echo json_encode($response);
	}

	public function result_bunching()
	{
		# code...
	}

}