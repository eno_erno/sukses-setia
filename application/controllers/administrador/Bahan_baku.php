<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'third_party/Spout/Autoloader/autoload.php';
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Bahan_baku extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Bangkok");
		$this->load->model('Basic_model', 'basic');
	}

	public function index()
	{
		$data['title'] = 'Stok <strong>Bahan Baku</strong>';
		$data['user'] = $this->db->get_where('user', 
			['username' => $this->session->userdata('username')])->row_array();

		$this->db->select('id, material_name, kode_supplier');
		$data['type_bahanbaku'] = $this->db->get_where('material_kawat_stok')->result_array(); 

		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/sidebar', $data);
		$this->load->view('backend/templates/topbar', $data);
		$this->load->view('backend/bahan-baku/index', $data);
		$this->load->view('backend/templates/footer');
	}

	public function ajax_laporan_material()
	{
		$result = [];

		$id = $this->input->post('id');
		$this->db->select('id, material_name, stok, slug, kode_supplier, total_bobin');
		$result['material_kawat_stok'] = $this->db->get_where('material_kawat_stok', ['id' => $id])->row_array();

		echo json_encode($result);
	}

	public function ajax_laporan_material_pvc()
	{
		$result = [];

		$id = $this->input->post('id');
		$this->db->select('id, stok');
		$result['material_pvc_stok'] = $this->db->get_where('material_pvc_stok', ['material_pvc_id' => $id])->row_array();

		echo json_encode($result);
	}

	public function ajax_laporan_pvc()
	{
		$result = [];

		$id = $this->input->post('id');
		$this->db->select('stok');
		$pvc_stok = $this->db->get_where('material_pvc_stok', 
			['material_pvc_id' => $id])->row_array();

		$this->db->select('pvc_name, slug');
		$pvc = $this->db->get_where('material_pvc', 
			['id' => $id])->row_array();

		$result['material_pvc_stok'] = [
			'stok' => $pvc_stok['stok'],
			'pvc_name' => $pvc['pvc_name'],
			'slug' => $pvc['slug']
		];

		echo json_encode($result);
	}

	public function ajax_bahankawat($id)
	{
		$result = array('data' => array());

		$data = $this->db->get_where('material_kawat_order', 
			['material_kawat_stok_id' => $id])->result_array();
		
		$no = 1;
		foreach ($data as $key => $value) :
			$modal = "showKeterangan('".$value['id']."')";

			#button action
			$buttons = '
					<a href="#" class="btn btn-outline-primary btn-sm" 
						data-toggle="modal" 
						data-target="#keteranganModal" onclick="'.$modal.'"><i class="fa fa-eye"></i></a> 
          <a href="'.site_url('administrador/bahan-baku/edit/'.$value['id']).'" class="btn btn-outline-light btn-sm"><i class="fa fa-edit"></i></a> 
				';

			$date = date('d.M Y', strtotime($value['tgl_order']));

			$result['data'][$key] = array(
				"<mark>#" .$no. "</mark> " .$date,
				$value['incoming_stok'],
				$value['stok_out'],
				$value['result_stok'],
				$buttons
			);

			$no++;
		endforeach;

		echo json_encode($result);
	}

	public function ajax_bahanpvc($id)
	{
		$result = array('data' => array());

		$data = $this->db->get_where('material_pvc_order', 
			['material_pvc_id' => $id])->result_array();

		$no = 1;
		foreach ($data as $key => $value) :
			$modal = "showKeterangan('".$value['id']."')";

			#button action
			$buttons = '
					<a href="#" class="btn btn-outline-primary btn-sm" 
						data-toggle="modal" 
						data-target="#keteranganModal" onclick="'.$modal.'"><i class="fa fa-eye"></i></a> 
          <a href="'.site_url('administrador/bahan-baku/edit/'.$value['id']).'" class="btn btn-outline-light btn-sm"><i class="fa fa-edit"></i></a> 
				';

			$date = date('d.M Y', strtotime($value['tgl_order']));

			$result['data'][$key] = array(
				"<mark>#" .$no. "</mark> " .$date,
				$value['incoming_stok'],
				$value['stok_out'],
				$value['result_stok'],
				$buttons
			);

			$no++;
		endforeach;

		echo json_encode($result);
	}

	public function ajax_ovendrum($no_machine)
	{
		$result = array('data' => array());

		$data = $this->db->get_where('material_oven_drum', 
			['status' => 0, 'no_machine' => $no_machine])
		->result_array();

		$no = 1;
		foreach ($data as $key => $value) :
			$modal = "lihatBobin('".$value['id']."')";
			$confirm = "return confirm('Are you sure update status this data?')";

			#button action
			$buttons = '
					<a href="#" class="btn btn-outline-primary btn-sm" 
						data-toggle="modal" 
						data-target="#lihatBobin" onclick="'.$modal.'"><i class="fa fa-eye"></i></a> 
          <a href="'.site_url('administrador/oven-drum/delete/'.$value['id']).'" onclick="'.$confirm.'" class="btn btn-outline-light btn-sm"> <i class="fa fa-trash"></i></a>
				';

			$date = date('d.M Y', strtotime($value['tgl_oven']));

			$result['data'][$key] = array(
				'<mark>'.$no. '</mark> ',
				'<p class="mb-0"><small>' .$date. '</small></p>'. $value['drawing'],
				$value['no_bobin'],
				$value['netto'],
				$buttons
			);

			$no++;
		endforeach;

		echo json_encode($result);
	}

	public function ajax_drawing()
	{
		$result = array('data' => array());

		$data = $this->db->get_where('material_oven_drum', ['status' => 0])->result_array();
		$no = 1;
		foreach ($data as $key => $value) :
			#button action
			$buttons = '
					<a href="#" class="btn btn-outline-primary btn-sm">Submit Oven</a> 
				';

			$result['data'][$key] = array(
				$value['no_bobin'],
				$value['bruto'],
				$value['berat_bobin'],
				$value['netto'],
				$buttons
			);

			$no++;
		endforeach;

		echo json_encode($result);
	}

	public function ajax_detailbahankawat()
	{
		$id = $this->input->post('id');

		if($id) :
			$this->db->select('id, no_order, information, operator, tgl_finish_production');
			$data = $this->db->get_where('material_kawat_order', ['id' => $id])->row_array();
			echo json_encode($data);
		endif;
	}

	public function ajax_detailbahanpvc()
	{
		$id = $this->input->post('id');

		if($id) :
			$this->db->select('id, no_order, information, operator');
			$data = $this->db->get_where('material_pvc_order', ['id' => $id])->row_array();
			echo json_encode($data);
		endif;
	}

	public function upload()
	{
		$config['upload_path']          = './assets/uploads'; #directory untuk menyimpan file
  	$config['allowed_types']        = 'xlsx|xls'; #jenis file

  	$this->upload->initialize($config);
  	if($this->upload->do_upload("import_file") ) :
  		$file = $this->upload->data();
  		$reader = ReaderEntityFactory::createXLSXReader();
  		$reader->open($file['full_path']);

  		foreach ($reader->getSheetIterator() as $sheet) :

  			$numRow = 1;
  			foreach ($sheet->getRowIterator() as $row) {
  				if($numRow > 1) {
  					var_dump($row->getCellAtIndex(1));
  				}

  				$numRow++;
  			}

  			$reader->close();
  			unlink('assets/uploads/' .$file['file_name']);
  		endforeach;
  	else:
  		$error = array('errors' => $this->upload->display_errors()); #show errornya
  		var_dump($error);
			$this->session->set_flashdata('error-upload', $error['errors']);
  	endif;
	}

	public function pvc()
	{
		$data['title'] = 'Stok <strong>Bahan Baku</strong>';
		$data['user'] = $this->db->get_where('user', 
			['username' => $this->session->userdata('username')])->row_array();

		$this->db->select('id, pvc_name, kode_pvc');
		$data['type_bahanbaku'] = $this->db->get('material_pvc')->result_array(); 

		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/sidebar', $data);
		$this->load->view('backend/templates/topbar', $data);
		$this->load->view('backend/bahan-baku/pvc', $data);
		$this->load->view('backend/templates/footer');
	}

	public function drawing()
	{
		$data['title'] = 'Tahap <strong>Drawing</strong>';
		$data['user'] = $this->db->get_where('user', 
			['username' => $this->session->userdata('username')])->row_array();

		$fullname = $data['user']['fullname'];
		$this->db->select('id, material_name, kode_supplier');
		$data['type_bahanbaku'] = $this->db->get_where('material_kawat_stok', ['position' => 'gudang'])->result_array(); 

		$this->form_validation->set_rules('tgl_order', 'Tgl Order', 'required');
		$this->form_validation->set_rules('barang_keluar', 'Barang Keluar', 'required');
		$this->form_validation->set_rules('information', 'Keterangan', 'required');
		$this->form_validation->set_rules('material_kawat_stok_id', 'Type Bahan', 'required');

		if($this->form_validation->run() === false) :
			$this->load->view('backend/templates/header', $data);
			$this->load->view('backend/templates/sidebar', $data);
			$this->load->view('backend/templates/topbar', $data);
			$this->load->view('backend/bahan-baku/drawing', $data);
			$this->load->view('backend/templates/footer');
		else:
			$post = $this->input->post();
			$id_material = $post['material_kawat_stok_id'];
			$stok_out = $post['barang_keluar'];

			$this->db->select('stok, total_bobin');
			$bahan_baku = $this->db->get_where('material_kawat_stok', ['id' => $id_material])->row(); 
			
			$result_stok = $bahan_baku->stok - $stok_out; 

			$dataOrder = [
				'material_kawat_stok_id' => $id_material,
				'no_order' => $post['no_order'],
				'incoming_stok' => '',
				'stok_out' => $stok_out,
				'result_stok' => $result_stok,
				'information' => $post['information'],
				'tgl_order' => $post['tgl_order'],
				'operator' => $fullname
			];

			$this->db->insert('material_kawat_order', $dataOrder);

			$this->db->select('id');
			$material = $this->db->get_where('material_kawat_stok', ['result_size' => $post['size']])->row();
			if($material) :
				$material_kawat_stok_id = $material->id;
			else: 
				$newMaterial = [
					'material_id' => 1,
					'kode_supplier' => 'SS',
					'stok' => 0,
					'size' => 'MM',
					'result_size' => $post['size'],
					'total_bobin' => 0,
					'material_name' => $post['size'] . " SS",
					'slug' => strtolower($post['no_order']) . "-sukses-setia",
					'position' => 'drawing' 
				];

				$this->db->insert('material_kawat_stok', $newMaterial);
				$material_kawat_stok_id = $this->db->insert_id();
			endif;

			$drawing = [
				'material_kawat_stok_id' => $material_kawat_stok_id,
				'no_bobin' => $post['no_bobin'],
				'material_drawing' => $post['size'] . " SS",
				'total_netto' => $stok_out,
				'drawing' => $post['drawing'],
				'operator' => $fullname
			];

			$this->db->insert('material_drawing', $drawing);

			$update_stok = [
				'stok' => $result_stok,
				'total_bobin' => $bahan_baku->total_bobin - 1
			];

			$update_status = [
				'status' => '2',
			];

			$this->basic->update('id', $id_material, $update_stok, 'material_kawat_stok');
			$this->basic->update('id', $post['material_kawat_id'], $update_status, 'material_kawat');

			#$this->basic->delete('id', $this->input->post('material_kawat_id', true), 'material_kawat');
			$this->session->set_flashdata("message", 'Order stok kawat keluar berhasil disimpan');
			redirect('administrador/bahan-baku');
		endif;
	}

	public function oven_drum()
	{
		$data['title'] = 'Pemanasan <strong>Bobin</strong>';
		$data['user'] = $this->db->get_where('user', 
			['username' => $this->session->userdata('username')])->row_array();

		$this->db->select('id, type_machine, no_machine');
		$data['type_mesin'] = $this->db->get('machine')->result(); 

		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/sidebar', $data);
		$this->load->view('backend/templates/topbar', $data);
		$this->load->view('backend/bahan-baku/oven-drum', $data);
		$this->load->view('backend/templates/footer');
	}

	public function bunching()
	{
		$data['title'] = 'Tahap <strong>Bunching</strong>';
		$data['user'] = $this->db->get_where('user', 
			['username' => $this->session->userdata('username')])->row_array();

		$fullname = $data['user']['fullname'];
		$this->db->select('id, material_name, kode_supplier');
		$data['type_bahanbaku'] = $this->db->get_where('material_kawat_stok', ['position' => 'gudang'])->result_array(); 

		$this->form_validation->set_rules('tgl_order', 'Tgl Order', 'required');
		$this->form_validation->set_rules('barang_keluar', 'Barang Keluar', 'required');
		$this->form_validation->set_rules('information', 'Keterangan', 'required');
		$this->form_validation->set_rules('material_kawat_stok_id', 'Type Bahan', 'required');

		if($this->form_validation->run() === false) :
			$this->load->view('backend/templates/header', $data);
			$this->load->view('backend/templates/sidebar', $data);
			$this->load->view('backend/templates/topbar', $data);
			$this->load->view('backend/bahan-baku/bunching', $data);
			$this->load->view('backend/templates/footer');
		else:
			$post = $this->input->post();
			$id_material = $post['material_kawat_stok_id'];
			$stok_out = $post['barang_keluar'];

			$this->db->select('stok, total_bobin');
			$bahan_baku = $this->db->get_where('material_kawat_stok', ['id' => $id_material])->row(); 
			
			$result_stok = $bahan_baku->stok - $stok_out; 
			$dataOrder = [
				'material_kawat_stok_id' => $id_material,
				'no_order' => $post['no_order'],
				'incoming_stok' => '',
				'stok_out' => $stok_out,
				'result_stok' => $result_stok,
				'information' => $post['information'],
				'tgl_order' => $post['tgl_order'],
				'operator' => $fullname
			];

			$this->db->insert('material_kawat_order', $dataOrder);

			$this->db->select('id');
			$material = $this->db->get_where('material_kawat_stok', ['result_size' => $post['size']])->row();
			if($material) :
				$material_kawat_stok_id = $material->id;
			else: 
				$newMaterial = [
					'material_id' => 1,
					'kode_supplier' => 'SS',
					'stok' => 0,
					'size' => 'MM2',
					'result_size' => $post['size'],
					'total_bobin' => 0,
					'material_name' => $post['size'] . " SS",
					'slug' => strtolower($post['no_order']) . "-sukses-setia",
					'position' => 'bunching' 
				];

				$this->db->insert('material_kawat_stok', $newMaterial);
				$material_kawat_stok_id = $this->db->insert_id();
			endif;

			$total_material = explode(", ", $post['material_kawat_id']);

			$update_stok = [
				'stok' => $result_stok,
				'total_bobin' => $bahan_baku->total_bobin - count($total_material)
			];

			$this->basic->update('id', $id_material, $update_stok, 'material_kawat_stok');

			foreach ($total_material as $value) :
				$update_status = [
					'status' => '3'
				];
			
				$this->basic->update('id', $value, $update_status, 'material_kawat');
			endforeach;
			
			$this->session->set_flashdata("message", 'Order stok kawat keluar berhasil disimpan');
			redirect('administrador/bahan-baku');
		endif;
	}

	public function coiling()
	{
		$data['title'] = 'Pembuatan <strong>Cable</strong>';
		$data['user'] = $this->db->get_where('user', 
			['username' => $this->session->userdata('username')])->row_array();

		$this->db->select('id, pvc_name, kode_pvc');
		$data['type_bahanbaku'] = $this->db->get('material_pvc')->result_array(); 

		$this->form_validation->set_rules('tgl_order', 'Tgl Order', 'required');
		$this->form_validation->set_rules('barang_keluar', 'Barang Keluar', 'required');
		$this->form_validation->set_rules('information', 'Keterangan', 'required');
		$this->form_validation->set_rules('material_pvc_id', 'Type Bahan', 'required');

		if($this->form_validation->run() === false) :
			$this->load->view('backend/templates/header', $data);
			$this->load->view('backend/templates/sidebar', $data);
			$this->load->view('backend/templates/topbar', $data);
			$this->load->view('backend/bahan-baku/export-pvc', $data);
			$this->load->view('backend/templates/footer');
		else:
			$idmaterial = $this->input->post('material_pvc_id', true);

			$this->db->select('stok');
			$stok_bahanbaku = $this->db->get_where('material_pvc_stok', ['id' => $idmaterial])->row('stok'); 

			$stok_out = $this->input->post('barang_keluar', true);

			$result_stok = $stok_bahanbaku - $stok_out; 

			$data = [
				'material_pvc_id' => $idmaterial,
				'no_order' => $this->input->post('no_order', true),
				'incoming_stok' => '',
				'stok_out' => $stok_out,
				'result_stok' => $result_stok,
				'information' => $this->input->post('information', true),
				'tgl_order' => $this->input->post('tgl_order', true),
				'operator' => $data['user']['fullname']
			];

			$this->db->insert('material_pvc_order', $data);

			$dataUpdate = [
				'stok' => $result_stok
			];

			$this->basic->update('id', $idmaterial, $dataUpdate, 'material_pvc_stok');
			$this->session->set_flashdata("message", 'Order stok pvc keluar berhasil disimpan');
			redirect('administrador/bahan-baku/pvc');
		endif;
	}

	public function bobin($material_kawat_stok_id)
	{
		$response = array('success' => false, 'messages' => array());

		$queryBobin = $this->db->query("
	    	SELECT id, no_bobin, netto, berat_bobin, bruto
	    	FROM material_kawat
	    	WHERE material_kawat_stok_id = '$material_kawat_stok_id'
	    	AND status = 1
	    	ORDER BY no_bobin ");

  	$bobins = $queryBobin->result_array();

    $response['success'] = true;
    $response['messages'] = '';
    $response['data'] = $bobins;

    echo json_encode($response);
	}

	public function select_bobin($material_kawat_id)
	{
		$response = array('success' => false, 'messages' => array());

		$sql_material_kawat = $this->db->query("
	    	SELECT id, no_bobin, netto
	    	FROM material_kawat
	    	WHERE id = '$material_kawat_id' ");

    $material_kawat = $sql_material_kawat->row();

    if(!empty($material_kawat)) :
    	$response['success'] = true;
    	$response['messages'] = '';
    	$response['data'] = $material_kawat;
    else:
    	$response['success'] = false;
    	$response['messages'] = 'material kawat id not found.';
    	$response['data'] = '';
    endif;

    echo json_encode($response);
	}
}