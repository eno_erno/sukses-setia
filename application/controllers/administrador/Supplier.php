<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Basic_model', 'menu');
	}

	public function index()
	{
		// is_logged_in();
		$data['title'] = 'Supplier';
		$data['user'] = $this->db->get_where('user', 
			['username' => $this->session->userdata('username')])->row_array();

		$this->form_validation->set_rules('kode_supplier', 'supplier Code', 'required|is_unique[supplier.kode_supplier]');
		$this->form_validation->set_rules('supplier_name', 'supplier Name', 'required');
		$this->form_validation->set_rules('number_phone', 'Phone Number', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('status', 'Active', 'required');

		if($this->form_validation->run() === false) :
			$this->load->view('backend/templates/header', $data);
			$this->load->view('backend/templates/sidebar', $data);
			$this->load->view('backend/templates/topbar', $data);
			$this->load->view('backend/supplier/index', $data);
			$this->load->view('backend/templates/footer');
		else:
			if($_FILES["avatar"]["name"] !== "") { 
				$this->set_upload();

				if($this->upload->do_upload("avatar") ) :
					$image = $this->upload->data();
					$url = $image['file_name'];	

					$data = [
						'kode_supplier' 	=> $this->input->post('kode_supplier', true),
						'name' 				=> $this->input->post('supplier_name', true),
						'number_phone' 		=> $this->input->post('number_phone', true),
						'avatar'			=> $url,
						'address' 			=> $this->input->post('address', true),
						'active' 			=> $this->input->post('status', true),
					];

					$this->db->insert('supplier', $data);
					$this->session->set_flashdata("message", '<div class="alert alert-success">New supplier Has Been saved.</div>');
					redirect('administrador/supplier#result');
				else:
					$data = [
						'kode_supplier' 	=> $this->input->post('kode_supplier', true),
						'name' 				=> $this->input->post('supplier_name', true),
						'number_phone' 		=> $this->input->post('number_phone', true),
						'address' 			=> $this->input->post('address', true),
						'active' 			=> $this->input->post('status', true),
					];

					$this->db->insert('supplier', $data);
					$this->session->set_flashdata("message", '<div class="alert alert-success">New supplier Has Been saved.</div>');

					redirect('administrador/supplier#result');
				endif;
			} else {
				$data = [
					'kode_supplier' 	=> $this->input->post('kode_supplier', true),
					'name' 				=> $this->input->post('supplier_name', true),
					'number_phone' 		=> $this->input->post('number_phone', true),
					'address' 			=> $this->input->post('address', true),
					'active' 			=> $this->input->post('status', true),
				];

				$this->db->insert('supplier', $data);
				$this->session->set_flashdata("message", '<div class="alert alert-success">New supplier Has Been saved.</div>');

				redirect('administrador/supplier#result');
			}

		endif;
	}

	public function getSupplier()
	{
		$result = array('data' => array());

		$data = $this->db->get('supplier')->result_array();
		$no = 1;
		foreach ($data as $key => $value) :
			$confirm = "return confirm('Are you sure delete this data?')";

			$buttons = '
					<a href="'.site_url('administrador/supplier/delete/'.$value['id']).'" class="badge badge-danger" onclick="'.$confirm.'">Delete</a>
					<a href="'.site_url('administrador/supplier/edit/'.$value['id']).'" class="badge badge-success">Edit</a>
				';

			if( $value['active'] == 0){
				$status = 'Not Active';
			}else{
				$status = 'Active';
			}

			if($value['updated_at'] != NULL){
				$date =  tgl_indo($value['updated_at']);

			}else{
				$date = NULL;
			}

			$result['data'][$key] = array(
				$no,
				$value['kode_supplier'],
				$value['name'],
				$value['number_phone'],
				$value['address'],
				$status,
				$date,
				$buttons
			);

			$no++;
		endforeach;

		echo json_encode($result);
	}

	public function edit($id = 0)
	{
		if($id == 0 && empty($id)) redirect("administrador/supplier");

		$supplier = $this->menu->first("supplier", 'id', $id); 
		if(empty($supplier)) redirect("administrador/supplier"); 

		$supplier = $supplier->row();
		$data = array('supplier' => $supplier);

		$data['title'] = 'Edit <strong>supplier</strong>';
		$data['user'] = $this->db->get_where('user', 
			['username' => $this->session->userdata('username')])->row_array();
		
		$original_value = $data['supplier']->kode_supplier;

		if($this->input->post('kode_supplier') != $original_value) {
		   $is_unique =  '|is_unique[supplier.kode_supplier]';
		} else {
		   $is_unique =  '';
		}

		$this->form_validation->set_rules('kode_supplier', 'supplier Code', 'required'.$is_unique);
		$this->form_validation->set_rules('supplier_name', 'supplier Name', 'required');
		$this->form_validation->set_rules('number_phone', 'Phone Number', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('status', 'Active', 'required');

		if($this->form_validation->run() === false) :
			$this->load->view('backend/templates/header', $data);
			$this->load->view('backend/templates/sidebar', $data);
			$this->load->view('backend/templates/topbar', $data);
			$this->load->view('backend/supplier/index', $data);
			$this->load->view('backend/templates/footer');
		else:
			if($_FILES["avatar"]["name"] !== "") { 
				$this->set_upload();

				if($this->upload->do_upload("avatar") ) :
					$image = $this->upload->data();
					$url = $image['file_name'];	

					$data = [
						'kode_supplier' 	=> $this->input->post('kode_supplier', true),
						'name' 				=> $this->input->post('supplier_name', true),
						'number_phone' 		=> $this->input->post('number_phone', true),
						'avatar'			=> $url,
						'address' 			=> $this->input->post('address', true),
						'active' 			=> $this->input->post('status', true),
						'updated_at'		=> date("Y-m-d H:i:s")
					];
				
					$this->db->update('supplier', $data, array('id' => $id)); #metode untuk update data.
					$this->session->set_flashdata("message", '<div class="alert alert-success">ID supplier <strong>'.$id.'</strong> updated</div>');
					redirect('administrador/supplier#result');
				else:
					$data = [
						'kode_supplier' 	=> $this->input->post('kode_supplier', true),
						'name' 				=> $this->input->post('supplier_name', true),
						'number_phone' 		=> $this->input->post('number_phone', true),
						'address' 			=> $this->input->post('address', true),
						'active' 			=> $this->input->post('status', true),
						'updated_at'		=> date("Y-m-d H:i:s")
					];

					$this->db->update('supplier', $data, array('id' => $id)); #metode untuk update data.
					$this->session->set_flashdata("message", '<div class="alert alert-success">ID supplier <strong>'.$id.'</strong> updated</div>');

					redirect('administrador/supplier#result');
				endif;
			} else {
				$data = [
					'kode_supplier' 	=> $this->input->post('kode_supplier', true),
					'name' 				=> $this->input->post('supplier_name', true),
					'number_phone' 		=> $this->input->post('number_phone', true),
					'address' 			=> $this->input->post('address', true),
					'active' 			=> $this->input->post('status', true),
					'updated_at'		=> date("Y-m-d H:i:s")
				];

				$this->db->update('supplier', $data, array('id' => $id)); #metode untuk update data.
				$this->session->set_flashdata("message", '<div class="alert alert-success">ID supplier <strong>'.$id.'</strong> updated</div>');

				redirect('administrador/supplier#result');
			}

		endif;
	}

	public function delete($id = 0)
	{
		if($id == 0 && empty($id)) redirect("administrador/supplier"); 

		$result = $this->menu->first("supplier", 'id', $id);
		if(empty($result)) redirect("administrador/supplier"); 
		$sup = $result->row();

		$filename = './assets/supplier/'. $sup->avatar;
		if (file_exists($filename)) :
	    	unlink($filename);
		endif;

		$this->session->set_flashdata("message", '<div class="alert alert-danger">ID supplier <strong>'.$id.'</strong> deleted</div>');
		$this->menu->delete('id', $id, 'supplier'); 
		redirect('administrador/supplier#result');
	}

	private function set_upload()
	{
		$config['upload_path']          = './assets/supplier'; #directory untuk menyimpan gambar/file
    	$config['allowed_types']        = 'png|jpg|jpeg|gif'; #jenis gambar
    	$config['max_size']             = 20480; // Limitasi 1 file image 10mb

    	$this->upload->initialize($config);
	}

	private function error_upload()
	{
		$error = array('errors' => $this->upload->display_errors()); #show errornya
		$this->session->set_flashdata('error-upload', '<div class="alert alert-danger">'.$error['errors'].'</div>');
	}

	public function getKodeSupplier($id)
	{
		if($id) : 
			$this->db->select('kode_supplier');
			$resultSupplier = $this->db->get_where("supplier", 
				['kode_supplier' => $id])->row_array();

			echo json_encode($resultSupplier);
		endif;
	}

	public function create()
	{
		if ($this->request->isAJAX()) :
			$TengGOClientID = $this->session->get('TengGO_Client_ID');
			$UserName = $this->session->get('User_Name');

			#get last id audit
			$last_id_audit = $this->transactionLastIdModel->Get_Last_ID_Transaction(); 

			$response = array('success' => false, 'messages' => array());

	    #validasi form.
	    $validate = $this->validate([
	      /*'client_id' => [
	        'label' => 'Client Code',
		      'rules' => 'required|my_is_unique[invoice_sign_catalog.client_code]'
	      ],*/
	      'client_code' => [
	        'label' => 'Client Code',
		      'rules' => 'required'
	      ],
	      'inv_sign' => [
	        'label' => 'Invoice Sign',
		      'rules' => 'required'
	      ],
	      'inv_jabatan' => [
	        'label' => 'Invoice Job Title',
		      'rules' => 'required'
	      ],
	      'valid_from' => [
	        'label' => 'Valid From',
		      'rules' => 'required|valid_this_date|valid_from_date'
	      ],
	      'valid_to' => [
	        'label' => 'Valid To',
		      'rules' => 'required|valid_this_date'
	      ]
	    ]);

	    if(!$validate) : #jika gagal
	      $validation = \Config\Services::validation();
	      $response['success'] = false;
	      foreach ($_POST as $key => $value) :
	        #render pesan error untuk setiap inputannya.
	        $response['messages'][$key] = $validation->getError($key);  
	      endforeach;
	    else:
	    	$post = $this->request->getPost();
	    	var_dump($post['client_code']);
	    	die;

	    	/*if(!empty($post['client_id'])) :
	    		$explode = explode(", ", $post['client_id']);
	    	endif;*/

	    	if(count($explode) > 1) :
	    		$client_code_array = [];

	    		foreach ($explode as $key => $value) :
	    			$cekClientCode = $this->invoiceSignModel->getCatalogInvoiceSign($TengGOClientID, $value);
	    			
	    			if($cekClientCode) :
	    				array_push($client_code_array, $value);
	    			else:
		    			$data = [
								'Client_Code' 		=> $value,
								'Inv_Sign' 		=> $post['inv_sign'],
								'Inv_Jabatan' 					  => $post['inv_jabatan'], 
								'F_Pajak_Sign' 						=> $post['f_pajak_sign'], 
								'F_Pajak_Jabatan' 						=> $post['f_pajak_jabatan'], 
								'BAP_Sign' 						=> $post['bap_sign'], 
								'BAP_Jabatan' 						=> $post['bap_jabatan'], 
								'BAP_Client_Sign' 						=> $post['bap_client_sign'], 
								'BAP_Client_Jabatan' 						=> $post['bap_client_jabatan'], 
								'Valid_From' 						=> $post['valid_from'] ." ". date("H:i:s"), 
								'Valid_To' 						=> $post['valid_to'] ." ". date("H:i:s"), 
						  	'TengGO_Client_ID' 	=> $TengGOClientID
			    		];

			    		$CatalogInvoiceSignMultiple = $this->invoiceSignModel->save($data);

							if($CatalogInvoiceSignMultiple) :
								foreach($data as $key => $item) :
									submit_audit_trails(
										$last_id_audit, 
										'invoice_sign_catalog', 
										$value,
										$key, 
										null,
										$item, 
										date('Y-m-d H:i:s'),
										$UserName,
										$TengGOClientID);
								endforeach;

								$this->transactionLastIdModel->Insert_Last_Number($last_id_audit, $TengGOClientID);
			      	endif;
			      endif;
	    		endforeach;

    			$response['success']  = true;
	        $response['messages'] = "Multiple data has been saved.";
	        $response['data'] = $client_code_array;
		    else:
		    	$data = [
						'Client_Code' 		=> $post['client_id'],
						'Inv_Sign' 		=> $post['inv_sign'],
						'Inv_Jabatan' 					  => $post['inv_jabatan'], 
						'F_Pajak_Sign' 						=> $post['f_pajak_sign'], 
						'F_Pajak_Jabatan' 						=> $post['f_pajak_jabatan'], 
						'BAP_Sign' 						=> $post['bap_sign'], 
						'BAP_Jabatan' 						=> $post['bap_jabatan'], 
						'BAP_Client_Sign' 						=> $post['bap_client_sign'], 
						'BAP_Client_Jabatan' 						=> $post['bap_client_jabatan'], 
						'Valid_From' 						=> $post['valid_from'] ." ". date("H:i:s"), 
						'Valid_To' 						=> $post['valid_to'] ." ". date("H:i:s"), 
				  	'TengGO_Client_ID' 	=> $TengGOClientID
		    	];

	      	$createCatalogInvoiceSign = $this->invoiceSignModel->save($data);

					if($createCatalogInvoiceSign) :
						foreach($data as $key => $value) :
							submit_audit_trails(
								$last_id_audit, 
								'invoice_sign_catalog', 
								$post['client_id'],
								$key, 
								null,
								$value, 
								date('Y-m-d H:i:s'),
								$UserName,
								$TengGOClientID);
						endforeach;

						$this->transactionLastIdModel->Insert_Last_Number($last_id_audit, $TengGOClientID);

		        $response['success']  = true;
		        $response['messages'] = "Data has been saved.";
		        $response['data']     = "";
	      	else:
	      		$response['success']  = false;
	        	$response['messages'] = $this->invoiceSignModel->error()['message'];
      		endif;
		    endif;

	    endif;

	    echo json_encode($response);
  	endif;
	}
}