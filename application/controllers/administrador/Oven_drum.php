<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oven_drum extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Bangkok");
		$this->load->model('Basic_model', 'basic');
	}

	public function submit()
	{
		$data['title'] = 'Catatan <strong>Drawing</strong>';
		$data['user'] = $this->db->get_where('user', 
			['username' => $this->session->userdata('username')])->row_array();

		$this->db->select('no_bobin, material_drawing, drawing, id, total_netto');
		$data['type_bahanbaku'] = $this->db->get_where('material_drawing', ['status' => 0])->result_array(); 

		$this->db->select('id, type_machine, no_machine');
		$data['type_mesin'] = $this->db->get('machine')->result(); 

		$this->form_validation->set_rules('material_name', 'Bobin Name', 'required');
		$this->form_validation->set_rules('no_bobin[]', 'No Bobin', 'required');
		$this->form_validation->set_rules('berat_bobin[]', 'Berat Bobin', 'required');
		$this->form_validation->set_rules('bruto[]', 'Bruto', 'required');

		if($this->form_validation->run() === false) :
			$this->load->view('backend/templates/header', $data);
			$this->load->view('backend/templates/sidebar', $data);
			$this->load->view('backend/templates/topbar', $data);
			$this->load->view('backend/oven-drum/submit', $data);
			$this->load->view('backend/templates/footer');
		else:
			$post = $this->input->post();

			$no_bobin = $post['no_bobin'];
			$berat_bobin = $post['berat_bobin'];
			$bruto = $post['bruto'];

			$this->db->select('no_bobin, drawing, material_kawat_stok_id');
			$drawing = $this->db->get_where('material_drawing', ['id' => $post['material_name']])->row_array();

			foreach ($no_bobin as $key => $value) {
				$data = [
					'material_kawat_stok_id' => $drawing['material_kawat_stok_id'],
					'from_bobin' => $drawing['no_bobin'],
					'drawing' => $drawing['drawing'],
					'no_bobin' => $value,
					'no_machine' => $post['no_machine'],
					'berat_bobin' => $berat_bobin[$key],
					'bruto' => $bruto[$key],
					'netto' => $bruto[$key]-$berat_bobin[$key],
					'status' => 0,
					'tgl_oven' => date('Y-m-d')
				];

				$this->db->insert('material_oven_drum', $data);
			}

			$update = [
				'status' => 1
			];

			$this->basic->update('id', $post['material_name'], $update, 'material_drawing'); 
			$this->session->set_flashdata("message", 'Proses oven sedang dilakukan');
			redirect('administrador/bahan-baku/oven-drum');
		endif;
	}

	public function delete($id)
	{
		if($id == 0 && empty($id)) redirect("administrador/bahan-baku/oven-drum"); 

		$result = $this->basic->first("material_oven_drum", 'id', $id);
		if(empty($result)) redirect("administrador/bahan-baku/oven-drum"); 

		$this->session->set_flashdata("message", 'bobin sudah <strong>'.$id.'</strong> di updated');
		
		$update = [
			'status' => 1
		];

		$this->basic->update('id', $id, $update, 'material_oven_drum'); 
		redirect('administrador/bahan-baku/oven-drum');
	}
}